<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/','MainPageController@index');
Route::get('/about', 'MainPageController@about');
Route::get('/overview','MainPageController@overview');
Route::get('/salient-features','MainPageController@slientfeatures');
Route::get('/internal-examination','MainPageController@internalExamination');
Route::get('/admission','MainPageController@admission');
Route::get('/faq','MainPageController@faq');
Route::get('/organizationalchart','MainPageController@organizationalchart');
Route::get('/testimonial','MainPageController@testimonial');
Route::get('/committe/rmc','MainPageController@rmc');
Route::get('/almuni-association','MainPageController@alumuniassociation');
Route::get('/committee','MainPageController@committee');
Route::get('/contact','MainPageController@contact');
Route::get('/campus-life','MainPageController@gallery');
Route::get('/program','MainPageController@program');
Route::get('/faculty','MainPageController@faculty');
Route::get('/news','MainPageController@news');
Route::get('/staff','MainPageController@staff');
Route::get('/notice','MainPageController@notice');
Route::get('/message-from-chariman','MainPageController@messagefromchairman');
Route::get('/message-from-principal','MainPageController@messagefromprincipal');
Route::get('/eventdetails/{id}','MainPageController@eventdetails');
Route::get('/newsdetails/{id}','MainPageController@newsdetails');
Route::get('/noticedetails/{id}','MainPageController@noticedetails');
Route::get('/programdetails/{id}','MainPageController@programdetails');
Route::get('/facultydetails/{id}','MainPageController@facultydetails');
Route::get('/research','MainPageController@publication');
Route::get('/historyofmtc','MainPageController@historyMtc');


Route::post('/sendemail/send', 'SendMailController@send');
Route::post('/sendemail/sendmail', 'SendMailController@sendmail');

Route::get('/home/gallery', 'GalleryController@index');
Route::get('/home/gallery/create', 'GalleryController@create');
Route::post('/home/gallery/create', 'GalleryController@store');
Route::get('/home/gallery/edit/{id}', 'GalleryController@edit')->name('gallery.edit');
Route::post('/home/gallery/edit/{id}', 'GalleryController@update')->name('gallery.update');
Route::delete('/home/gallery/destroy/{id}', 'GalleryController@destroy')->name('gallery.delete');

Route::get('/home/testimonials', 'TestimonialController@index');
Route::get('/home/testimonials/create', 'TestimonialController@create');
Route::post('/home/testimonials/create', 'TestimonialController@store');
Route::get('/home/testimonials/edit/{id}', 'TestimonialController@edit')->name('testimonial.edit');
Route::post('/home/testimonials/edit/{id}', 'TestimonialController@update')->name('testimonial.update');
Route::delete('/home/testimonials/destroy/{id}', 'TestimonialController@destroy')->name('testimonial.delete');

Route::get('/home/programs', 'ProgramController@index');
Route::get('/home/programs/create', 'ProgramController@create');
Route::post('/home/programs/create', 'ProgramController@store');
Route::get('/home/programs/edit/{id}', 'ProgramController@edit')->name('program.edit');
Route::post('/home/programs/edit/{id}', 'ProgramController@update')->name('program.update');
Route::delete('/home/programs/destroy/{id}', 'ProgramController@destroy')->name('program.delete');

Route::get('/home/news', 'NewsController@index');
Route::get('/home/news/create', 'NewsController@create');
Route::post('/home/news/create', 'NewsController@store');
Route::get('/home/news/edit/{id}', 'NewsController@edit')->name('news.edit');
Route::post('/home/news/edit/{id}', 'NewsController@update')->name('news.update');
Route::delete('/home/news/destroy/{id}', 'NewsController@destroy')->name('news.delete');

Route::get('/home/slider', 'SliderController@index');
Route::get('/home/slider/create', 'SliderController@create');
Route::post('/home/slider/create', 'SliderController@store');
Route::get('/home/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
Route::post('/home/slider/edit/{id}', 'SliderController@update')->name('slider.update');
Route::delete('/home/slider/destroy/{id}', 'SliderController@destroy')->name('slider.delete');

Route::get('/home/staff', 'StaffController@index');
Route::get('/home/staff/create', 'StaffController@create');
Route::post('/home/staff/create', 'StaffController@store');
Route::get('/home/staff/edit/{id}', 'StaffController@edit')->name('staff.edit');
Route::post('/home/staff/edit/{id}', 'StaffController@update')->name('staff.update');
Route::delete('/home/staff/destroy/{id}', 'StaffController@destroy')->name('staff.delete');

Route::get('/home/event', 'EventController@index');
Route::get('/home/event/create', 'EventController@create');
Route::post('/home/event/create', 'EventController@store');
Route::get('/home/event/edit/{id}', 'EventController@edit')->name('event.edit');
Route::post('/home/event/edit/{id}', 'EventController@update')->name('event.update');
Route::delete('/home/event/destroy/{id}', 'EventController@destroy')->name('event.delete');

Route::get('/home/faculty', 'FacultyController@index');
Route::get('/home/faculty/create', 'FacultyController@create');
Route::post('/home/faculty/create', 'FacultyController@store');
Route::get('/home/faculty/edit/{id}', 'FacultyController@edit')->name('faculty.edit');
Route::post('/home/faculty/edit/{id}', 'FacultyController@update')->name('faculty.update');
Route::delete('/home/faculty/destroy/{id}', 'FacultyController@destroy')->name('faculty.delete');

Route::get('/home/notice', 'NoticeController@index');
Route::get('/home/notice/create', 'NoticeController@create');
Route::post('/home/notice/create','NoticeController@store');
Route::get('/home/notice/edit/{id}', 'NoticeController@edit')->name('notice.edit');
Route::post('/home/notice/edit/{id}','NoticeController@update')->name('notice.update');
Route::delete('/home/notice/destroy/{id}','NoticeController@destroy')->name('notice.delete');

Route::get('/home/alumuni','AlumuniController@index');
Route::get('/home/alumuni/create', 'AlumuniController@create');
Route::post('/home/alumuni/create', 'AlumuniController@store');
Route::get('/home/alumuni/edit/{id}', 'AlumuniController@edit')->name('alumuni.edit');
Route::post('/home/alumuni/edit/{id}', 'AlumuniController@update')->name('alumuni.update');
Route::delete('/home/alumuni/destroy/{id}', 'AlumuniController@destroy')->name('alumuni.delete');

Route::get('/home/publication','PublicationController@index');
Route::get('/home/publication/create', 'PublicationController@create');
Route::post('/home/publication/create', 'PublicationController@store');
Route::get('/home/publication/edit/{id}', 'PublicationController@edit')->name('publication.edit');
Route::post('/home/publication/edit/{id}', 'PublicationController@update')->name('publication.update');
Route::delete('/home/publication/destroy/{id}', 'PublicationController@destroy')->name('publication.delete');

Route::get('/home/modal', 'PopupController@index');
Route::get('/home/modal/create', 'PopupController@create');
Route::post('/home/modal/create', 'PopupController@store');
Route::get('/home/modal/edit/{id}', 'PopupController@edit')->name('modal.edit');
Route::post('/home/modal/edit/{id}', 'PopupController@update')->name('modal.update');
Route::delete('/home/modal/destroy/{id}', 'PopupController@destroy')->name('modal.delete');