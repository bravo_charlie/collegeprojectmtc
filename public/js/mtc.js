jQuery(document).ready(function($){



/*Contact Form Submission*/

$('#contactform').on('submit',function(e){

	
	e.preventDefault();

	$('.js-show-feedback').removeClass('js-show-feedback');
	
	var form = $(this);

	var name = form.find('#name').val(),

		
		email = form.find('#email').val(),

		
		subject = form.find('#subject').val(),
		
		message = form.find('#message').val(),

		ajaxurl = form.data('url');

	
		if (name === ''  || email=== ''  || subject === '' || message === '') {

			console.log('Requires Inputs are not available');

			return;

		}

		form.find('input,button,textarea').attr('disabled','disabled');
		$('.js-form-submission').addClass('js-show-feedback');


		$.ajax({

			url : ajaxurl,
			type : 'post',
			data: {

				name : name,
				
				email : email,
				
				subject : subject,
				message : message,
				action : 'mtc_save_user_contact_form'
			},

			error: function(response){

				$('.js-show-feedback').removeClass('js-show-feedback');
				$('.js-form-error').addClass('js-show-feedback');
				form.find('input,button,textarea').removeAttr('disabled');
			},

			success: function(response){

				if (response == 0) {

				setTimeout(function(){


				$('.js-show-feedback').removeClass('js-show-feedback');
				$('.js-form-error').addClass('js-show-feedback');
				form.find('input,button,textarea').removeAttr('disabled');


				},1500);

		
				


				}
				else{

				setTimeout(function(){

				$('.js-show-feedback').removeClass('js-show-feedback');
				$('.js-form-success').addClass('js-show-feedback');
				form.find('input,button,textarea').removeAttr('disabled').val('');



				},1500);


				

				

				}
			}

		
		});






});












});