window.onscroll = function () {
    stickyFunction()
};

function stickyFunction() {
    var navbar = document.getElementById("header");
    var sticky = $("#header").outerHeight();

    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
}