@extends('layouts.app')

@section('content')
<section>
                <div class="about-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="about-banner">
                                    <h2>Message from the Chairman</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="breadcrumb-wrapper">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active"
                                    aria-current="page">Message from the Chairman</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </section>

 <section class="mt-30">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="students-desc-title">
                                <h3>Message from the Chairman</h3>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="students-desc">                  
<div class="wp-block-image">
	<figure class="alignleft is-resized">
		<img src="/images/Kishore-Maharjan-1.jpg" alt="Basanta" class="wp-image-210" width="499" height="330"/>
		<figcaption style="color:#f37021;"><strong>Kishore K. Maharjan<br>Executive Chairman</strong></figcaption>
	</figure>
</div>
<p> <strong>Welcome to Modern Technical College!</strong> 
</p>
<p>It’s my pleasure to send out this warm greeting to all the students, aspiring students, 
the community of Modern Technical College (MTC) and all it’s well-wishers. MTC is an
 institution serving the nation by enabling young citizens of the country grow up to 
 become young professionals in the medical and health sector through various graduate 
 programs covering Medical Lab Technology, Pharmacy and Public Health. 
</p>
<p>MTC by now carries a glorious history of producing hundreds of students who are
 presently working in various medical related institutions of the country and abroad too.
  As time goes by, MTC alumni will shine in many strata of the Nepalese society and beyond.
</p>
<p>Under the full ownership of Star Hospital Limited, one of the most promisingly upcoming
 hospitals of Nepal, MTC students have unique opportunity of practicing their classroom 
 theories in the related practical environment. Many can find their way to into the 
 hospital’s ranks as bonafide employees.
</p>
<p>The students at MTC have the opportunity of studying under dedicated faculty members
 lead by its dynamic Principal, Mr. Govinda Akela Paudel, and training in numerous 
 specialized laboratories besides On The Job Training opportunity at Star Hospital. 
 And of course, the doors of the Hospital will always be open, as required, for anyone 
 aspiring for professional future career in hospital sector.
</p>
<p>Star Hospital is 100 bed hospital operating in property created for hospital operation 
only and hence has world class logistic standards required for hospital of this size.
Star Hospital is class apart with 3 meter wide corridors, 10 feet high ceiling, 
3 modular system OTs with HEPA Filter air circulation system, fully airconditioned 
wards (including General Wards), very impressive and airy Emergency Room. With the
planned introduction of State of the Art 10,000 sq. feet G.I. Center and 3,000 sq ft 
IVF Center (JV with Dynamic Fertility of India), advanced cosmetic surgery, 
ophthalmology, urology, gynecology/pediatrics (including NICU Ward), neurology, 
orthopedics, etc. Star will shine brighter in the days to come. All these will be 
very promising opportunities for the graduates of MTC to grab out for as they 
complete their studies.
</p>
<p>As MTC is working on introducing Masters programs (MPH and MPharma), the prospect for 
    our students climbing much higher is very attractive. Further, as two of the 
    Hospital’s promoter groups run two pharmaceutical companies, there is additional 
    value addition for the pharmaceutical program students.
</p>
<p>MTC is a very blessed institution situated right adjacent to Star Hospital along the
 Ring Road in Sanepa. The college has excellent academic and clinical laboratory
  facilities for each program. The Curriculum has been revised and updated to 
  international standards. As the developing nations of the world struggle to 
  maintain adequate and proper health care system for their citizens, demand for 
  professionals educated in institutions like MTC will keep on growing for many more 
  decades to come.
</p>
<p>I must reiterate here that the benefits that can be imparted to students are immense 
    and hence every student must within oneself excel and grab the opportunity for a 
    bright and blessed career in future. At MTC, I assure all the students that this 
    institution will do everything possible to realize your dreams.
</p>

                         </div>
                        </div>
                      </div>
                </div>
            </section>





@endsection