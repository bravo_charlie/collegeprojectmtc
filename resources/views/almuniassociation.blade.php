@extends('layouts.app')

@section('content')
 <section>
        <div class="about-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="about-banner">
                            <h2>Alumni Association</h2>
                        </div>
                     </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="breadcrumb-wrapper">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Alumni Association</li>
                    </ol>
                </nav>
            </div>
        </div>
</section>
<section class="mt-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="students-desc-title">
                    <h3>Alumni Association</h3>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="students-desc">
                                                 
<p>MTC Alumni Association is the organization of the alumni of the school since its establishment.
In order to form the executive committee of the alumni recently an alumni meet was held which formed an ad hoc committee
 under the leadership of 
Dr. Chandan Shrestha, who was the BPharm graduate from the school in 2005.<br>
</p>
<ul class="wp-block-gallery columns-3 is-cropped">
    @foreach($alumunis as $alumuni)
	<li class="blocks-gallery-item">
		<figure>
			<img src="../uploads/alumuni/{{$alumuni->image}}" alt="" data-id="328" sizes="(max-width: 960px) 100vw, 960px" />
			<figcaption>{{$alumuni->figurecaption}}</figcaption>
		</figure>
	</li>
     @endforeach
	</ul>
     <div class="row page_nav_row">
            <div class="col">
                <div class="page_nav">
                    <ul class="d-flex flex-row align-items-center justify-content-center">
                        <li style="list-style:none;"> {{ $alumunis->links() }}</li>
                    </ul>
                </div>
            </div>
        </div>
  </div>
    </div>
      </div>
        </div>
          </section>
@endsection