@extends('layouts.app')

@section('content')

        <section>
                <div class="about-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="about-banner">
                                    <h2>Internal Examination</h2>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="breadcrumb-wrapper">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active"
                                    aria-current="page">Internal Examination</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </section>
            
 <section class="mt-30">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="students-desc-title">
                                <h3>Internal Examination</h3>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="students-desc">
                                
<b>Examination System</b>
<p>The examination system in Modern Technical College is based on Pokhara University examination system, where a student’s academic performance in a course is evaluated in two phases:
</p>
<b>Phase I: Internal Assessment</b>
<p>Assessment of student’s performance is  evaluated by the faculty member through quizzes, tutorials, lab works, home assignments, class tests, class participation, term papers, internal exam etc
Internal examination is conducted by the concerned program consisting of three examinations in each semester;</p> 

<p><b>First Internal (carrying weight of 30  marks) ,</b></p> 
<p>Second Internal ( carrying weight of 50  marks) and Pre-board examination ( carrying weight of 100 marks , which is similar to board examination pattern)
A fifty percent weight is given to internal evaluation, which consists of weighted average of all score obtained in first, second and pre-board examination as well as their other performance based assessment.
</p>
<b>Phase II: External Assessment</b>
<p>External assessment of student’s performance is evaluated by the Office of the Controller of Examinations of Pokhara University through semester-end examinations (board examination)
Fifty percent weight is given to external evaluation (semester-end examination)</p>

<p>A student is required to pass the internal and external evaluations independently. The final grade awarded to a student in a course is based on his/her consolidated performance in both internal and external evaluations.
A student will get NOT QUALIFIED (NQ) status in the internal evaluation if his/her performance falls below the minimum requirement. Such students will not be allowed to appear in the semester-end examination of that particular course.
</p>






                                                            </div>
                        </div>
                                            </div>
                </div>
            </section>




  @endsection