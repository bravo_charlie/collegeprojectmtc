@extends('layouts.app')

@section('content')
<section>
        <div class="about-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="about-banner">
                            <h2>Event</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<section class="bg-light">
    <div class="container">
        <div class="breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">Home</a>
                    </li>
                    <li class="breadcrumb-item ">
                        <a href="#">Event</a>
                    </li>
                    <li class="breadcrumb-item active"
                            aria-current="page">{{$events->title}}</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
 <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                     <div class="event-details-wrapper">
                         <img class="card-img-top" src="/uploads/events/{{$events->image}}"
                                                 alt="Card image cap">
          
                    <div class="event-details">
                        <h2>{{$events->title}}</h2>
                        <p class="event-date">
                         <span><i class="fa fa-calendar-alt">{{$events->created_at->format('Y , M d ')}}</i></span>
                         <span><i class="fa fa-clock"></i>{{$events->created_at->format('h:i:sa')}}</span>
                        </p>
                            <p class="event-text">
                             <div dir="auto"><?php echo ($events->description ); ?></div>
                            </p>
                        </div>
                    </div>
                   
             </div>
            </div>
         </div>
</section>


@endsection