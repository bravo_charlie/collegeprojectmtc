@extends('layouts.app')

@section('content')

 <section>
                <div class="about-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="about-banner">
                                    <h2>Admission</h2>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="breadcrumb-wrapper">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active"
                                    aria-current="page">Admission</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </section>
            
            <section class="mt-30">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="students-desc-title">
                                <h3>Admission</h3>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="students-desc">
                             <h4><strong>Admission Procedure</strong></h4>
<p>Eligibility of the candidates is assessed on the bases of minimum requirements to apply for the specific courses mentioned above. Eligible applicants have to appear the entrance examination held of faculty of health sciences (Students are enrolled in the undergraduate program in September session and postgraduate programs in March session  Entrance examinations for the program are organized with the objective questions of three hours duration. Meritorious students who successfully passed the entrance examination are admitted into the respective program on merit orders in the defined seats.</p>

                                                            </div>
                        </div>
                                            </div>
                </div>
            </section>
  @endsection