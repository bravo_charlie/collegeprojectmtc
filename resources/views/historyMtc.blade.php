@extends('layouts.app')

@section('content')
<div id="content" role="main" class="span8 offset2">

    <section>
        <div class="about-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="about-banner">
                            <h2>Overview</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="breadcrumb-wrapper">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active"
                        aria-current="page">Overview</li>
                    </ol>
                </nav>
            </div>
        </div>
    </section>

    <section class="mt-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="students-desc-title">
                        <h3>Overview</h3>
                    </div>
                </div>
                <div class="col-lg-12">
                Modern Technical College is an educational institute affiliated to Pokhara University (PoU). It's a unique college run by prominent senior doctors practising in various departments in different hospitals of the country. The academic course is being headed by senior Academicians who has a decade of academic and work experience in the field of Health Science. It has its own multi-speciality 100 bedded STAR Hospital specially designed for academic purpose having different Departments of Medical Sciences where students can execute clinical practises. In fact, this is a very wonderful college especially dedicated to its students, Our college's motto is 'SERVICE with SOUND knowledge'!!! The college was registered in Company registrar office of the Government of Nepal on 2067 B.S. (Registration number: 74085/066/067, PAN number: 304270725).
                    </div>
                </div>
            </div>
        </section>
     </div>

@endsection