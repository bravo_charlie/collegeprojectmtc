@extends('layouts.app')

@section('content')

        <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">ABOUT US</h2>
                        </div>
                    </div>
                </div>
            </div>                  
        </div>



        <section class="main-content course-single">
            <div class="container">
                <div class="content-course">
                    <div class="row">
                        <div class="col-md-9">
                            <article class="post-course">
                                <div class="row">
               
                                    
                                    <div class="col-md-1 col-sm-5">
                                        <div class="content-pad single-event-meta">
                                            <div class="item-thumbnail">
                                         </div>
                                        </div>
                                    </div>
                         <div class="col-md-8">
                                        <div class="content-pad single-course-detail">
                                            <div class="course-detail">
                                               
                                                    <div class="title-list v1">
                                     
                                    </div><!-- /title-list -->

                                                <div class="content-content">
                                                    <div class="content-dropcap v1">
                                                        <p>Modern Technical College(MTC) is one of the growing institutions for producing level human resource of Allied health sciences under the umbrella of Star Hospital. MTC is affiliated to Pokhara University and managed by academicians,researchers,industrialists, entrepreneurs and prominent professional with extensive exposure to a wide range of knowledge,experience and teaching/learning practices. The institution is commited to provide quality,practical and real life based education and aims to be A CENTRE OF EXCELLENCE through quality education, research and services.
Four years bachelor o Medical Laboratary Science program(BSc MLT) has been running at MTC since 2010. We are proud to announce that our 1st, 2nd and 3rd batch students have recently graduated. We are devoted to produce skillful, market oriented and competent graduate who can contribute in delivering the diagnostic services efectively. </p>

                                                        
                                                    </div>

                                                                               
                                                </div><!--/content-content-->
                                           

                                            </div><!--/course-detail-->
                                        </div><!--/single-content-detail-->         
                                    </div>


                                </div>
                            </article>
                        </div>

                        <div class="col-md-3">
                        <div class="sidebar">
                            <div class="widget widget-posts">
                                <h2 class="widget-title">RECENT EVENTS LIST</h2>
                                <ul class="recent-posts clearfix">
                                   <li>
                                        <div class="thumb item-thumbnail">
                                            <a href="#">
                                                <img width="55" height="80" src="/images/21151208_1644838168880629_7243431609911651545_n.jpg" class="attachment-square-thumbnail size-square-thumbnail wp-post-image" alt="" srcset="http://mtc.edu.np/wp-content/uploads/2019/04/21151208_1644838168880629_7243431609911651545_n.jpg 663w, http://mtc.edu.np/wp-content/uploads/2019/04/21151208_1644838168880629_7243431609911651545_n-207x300.jpg 207w" sizes="(max-width: 55px) 100vw, 55px" />                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                <div class="thumbnail-hoverlay-cross"></div>
                                            </a>
                                        </div>
                                        <div class="text">
                                            <a href="#">Result</a>
                                            <p>April 10, 2019</p>
                                        </div>
                                    </li>
                                   <li>
                                        <div class="thumb item-thumbnail">
                                            <a href="#">
                                                <img width="80" height="80" src="/images/admission-open-Recovered-400-80x80.png" class="attachment-square-thumbnail size-square-thumbnail wp-post-image" alt="" srcset="http://mtc.edu.np/wp-content/uploads/2019/04/admission-open-Recovered-400-80x80.png 80w, http://mtc.edu.np/wp-content/uploads/2019/04/admission-open-Recovered-400-150x150.png 150w, http://mtc.edu.np/wp-content/uploads/2019/04/admission-open-Recovered-400-263x263.png 263w" sizes="(max-width: 80px) 100vw, 80px" />                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                <div class="thumbnail-hoverlay-cross"></div>
                                            </a>
                                        </div>
                                        <div class="text">
                                            <a href="#">Announcement!!</a>
                                            <p>April 10, 2019</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="thumb item-thumbnail">
                                            <a href="#">
                                                <img width="61" height="80" src="/images/42992895_2151729378191503_3506030933855698944_n.jpg" class="attachment-square-thumbnail size-square-thumbnail wp-post-image" alt="" srcset="http://mtc.edu.np/wp-content/uploads/2019/04/42992895_2151729378191503_3506030933855698944_n.jpg 737w, http://mtc.edu.np/wp-content/uploads/2019/04/42992895_2151729378191503_3506030933855698944_n-230x300.jpg 230w" sizes="(max-width: 61px) 100vw, 61px" />                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                <div class="thumbnail-hoverlay-cross"></div>
                                            </a>
                                        </div>
                                        <div class="text">
                                            <a href="#">Entrance Result</a>
                                            <p>April 10, 2019</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="thumb item-thumbnail">
                                            <a href="#">
                                                <img width="56" height="80" src="/images/45805332_2211479885549785_2202143476278099968_n.jpg" class="attachment-square-thumbnail size-square-thumbnail wp-post-image" alt="" srcset="http://mtc.edu.np/wp-content/uploads/2019/04/45805332_2211479885549785_2202143476278099968_n.jpg 528w, http://mtc.edu.np/wp-content/uploads/2019/04/45805332_2211479885549785_2202143476278099968_n-211x300.jpg 211w" sizes="(max-width: 56px) 100vw, 56px" />                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                <div class="thumbnail-hoverlay-cross"></div>
                                            </a>
                                        </div>
                                        <div class="text">
                                            <a href="#">New Classes Starting</a>
                                            <p>April 10, 2019</p>
                                        </div>
                                    </li>
                                  <li>
                                        <div class="thumb item-thumbnail">
                                            <a href="#">
                                                <img width="80" height="80" src="/images/college.jpg" class="attachment-square-thumbnail size-square-thumbnail wp-post-image" alt="" srcset="http://mtc.edu.np/wp-content/uploads/2019/04/college.jpg 263w, http://mtc.edu.np/wp-content/uploads/2019/04/college-150x150.jpg 150w" sizes="(max-width: 80px) 100vw, 80px" />                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                <div class="thumbnail-hoverlay-cross"></div>
                                            </a>
                                        </div>
                                        <div class="text">
                                            <a href="#">Admission Open</a>
                                            <p>April 10, 2019</p>
                                        </div>
                                    </li>
                               </ul><!-- /popular-news clearfix -->
                            </div><!-- /widget-posts -->
                        </div><!-- sidebar -->
                    </div><!-- /col-md-3 -->
                </div>
                </div>
            </div>
        </section>



@endsection