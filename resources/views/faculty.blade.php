@extends('layouts.app')

@section('content')
<section>
    <div class="about-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="about-banner">
                        <h2>Faculty</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Faculty</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row"> 
        <div class="col-sm-12 students-desc-title">
        <h3>BPH FACULTY</h3>
        </div>
            @foreach($bph_faculty as $faculty)
            <div class="col-lg-4 col-sm-6">
                <div class="single-member card-deck eqheight">
                    <div class="card shadow">
                        <img width="200" height="200" class="card-img-top" src="../uploads/faculty/{{$faculty->image}}"
                         alt="" sizes="(max-width: 200px) 100vw, 200px" />
                             <div class="card-body">
                                <h5 class="card-title">{{$faculty->facultyname}}</h5>
                                 <p><?php echo ($faculty->facultyposition ) ?></p>
                                 <a href="/facultydetails/{{$faculty->id}}">
                                    <button type="button" class="btn btn-more mt-2">Details</button>
                                 </a>
                               </div>
                            </div>
                        </div>
                        
                    </div>
             @endforeach
       </div>

       <div class="row"> 
       <div class="col-sm-12 students-desc-title">
        <h3>BMLT FACULTY</h3>
        </div>
            @foreach($bscmlt_faculty as $faculty)
            <div class="col-lg-4 col-sm-6">
                <div class="single-member card-deck eqheight">
                    <div class="card shadow">
                        <img width="200" height="200" class="card-img-top" src="../uploads/faculty/{{$faculty->image}}"
                         alt="" sizes="(max-width: 200px) 100vw, 200px" />
                             <div class="card-body">
                                <h5 class="card-title">{{$faculty->facultyname}}</h5>
                                 <p><?php echo ($faculty->facultyposition ) ?></p>
                                 <a href="/facultydetails/{{$faculty->id}}">
                                    <button type="button" class="btn btn-more mt-2">Details</button>
                                 </a>
                               </div>
                            </div>
                        </div>
                        
                    </div>
             @endforeach
       </div>

       <div class="row"> 
       <div class="col-sm-12 students-desc-title">
        <h3>BPHARMA FACULTY</h3>
        </div>
            @foreach($bpharm_faculty as $faculty)
            <div class="col-lg-4 col-sm-6">
                <div class="single-member card-deck eqheight">
                    <div class="card shadow">
                        <img width="200" height="200" class="card-img-top" src="../uploads/faculty/{{$faculty->image}}"
                         alt="" sizes="(max-width: 200px) 100vw, 200px" />
                             <div class="card-body">
                                <h5 class="card-title">{{$faculty->facultyname}}</h5>
                                 <p><?php echo ($faculty->facultyposition ) ?></p>
                                 <a href="/facultydetails/{{$faculty->id}}">
                                    <button type="button" class="btn btn-more mt-2">Details</button>
                                 </a>
                               </div>
                            </div>
                        </div>
                        
                    </div>
             @endforeach
       </div>
    
      </div>
   </section>
@endsection
