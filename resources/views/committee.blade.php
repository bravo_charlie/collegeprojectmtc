@extends('layouts.app')

@section('content')

<section>
    <div class="about-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="about-banner">
                        <h2>Committee</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Committee</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
 <section class="mt-30">
        <div class="container">
                            <div class="row">
                    <div class="col-lg-12">
                        <div class="about-desc-title">
                            <h3><a href="/committee/rmc">Research Management Cell</a></h3>
                        </div>
                    </div>
                                            <div class="col-lg-12">
                            <div class="about-desc">
                                <div class="container">
                                    <div class="table-responsive-sm">
                                        <table class="table">
                                            <thead class="thead-dark">
                                            <tr>
                                                <th>S NO.</th>
                                                <th>Name</th>
                                                <th>Position</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Basant Chandra Marhatha</td>
                                                    <td> Chairman</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    
                </div>
                            <div class="row">
                    <div class="col-lg-12">
                        <div class="about-desc-title">
                            <h3><a href="/committee/rmc/">Research Management Cell</a></h3>
                        </div>
                    </div>
                                            <div class="col-lg-12">
                            <div class="about-desc">
                                <div class="container">
                                    <div class="table-responsive-sm">
                                        <table class="table">
                                            <thead class="thead-dark">
                                            <tr>
                                                <th>S NO.</th>
                                                <th>Name</th>
                                                <th>Position</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Dr. Dev Kumar Neupane</td>
                                                    <td>Director</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Mr. Navaraj Pokhrel</td>
                                                    <td>Director</td>
                                                </tr>
                                                 <tr>
                                                    <td>3</td>
                                                    <td>Mr.Govinda Akela Paudel</td>
                                                    <td>Principal</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    
                </div>
                    </div>

    </section>


@endsection