@extends('layouts.app')

@section('content')
<section>
        <div class="about-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="about-banner">
                            <h2>{{$programs->programname}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<section class="bg-light">
    <div class="container">
        <div class="breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">Home</a>
                    </li>
                    <li class="breadcrumb-item ">
                        <a href="#">Programs</a>
                    </li>
                    <li class="breadcrumb-item active"
                            aria-current="page">{{$programs->programname}}</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
 <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                     <div class="event-details-wrapper">
                         <img style="height:300px;width:100%;object-fit:cover;" class="card-img-top" src="/uploads/programs/{{$programs->image}}"
                            alt="Card image cap">
             </div>
                    <div class="event-details">
                        <h2>{{$programs->programname}}</h2>
                        <!-- <p class="event-date">
                         <span><i class="fa fa-calendar-alt">{{$programs->created_at->format('Y , M d ')}}</i></span>
                         <span><i class="fa fa-clock"></i>{{$programs->created_at->format('h:i:sa')}}</span>
                        </p> -->
                            <p class="event-text">
                             <div class="auto"><b>Qualification : &nbsp<?php echo ($programs->qualification ); ?></b></div>
                            </p>
                            <p class="event-text">
                             <div class="auto"><?php echo ($programs->description ) ?></div>
                            </p>
                    </div>
                 
                </div>
            </div>
         </div>
</section>


@endsection