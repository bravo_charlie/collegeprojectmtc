@extends('layouts.app')

@section('content')

<section>
    <div class="about-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="about-banner">
                        <h2>Contact</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contact</li>
                </ol>
            </nav>
        </div>
    </div>
</section>

<section>
    <div class="contact-info-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-wrapper shadow mb-3">
                        <div class="contact-info pt-3">
                            <ul>
                                <li>
                                    <i class="fa fa-phone contact-circle-icon"></i>
                                    <div class="info-text">
                                        <h5>Telephone</h5>
                                        <p><a href="tel:+977-01-5555630">+977-01-5555630</a></p>
                                        <p><a href="tel:+977 9841812980">+977 9841812980</a></p>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-envelope contact-circle-icon"></i>
                                    <div class="info-text">
                                        <h5>Email</h5>
                                        <p>mtechcollege@gmail.com</p>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-map-marker contact-circle-icon"></i>
                                    <div class="info-text">
                                        <h5>Address</h5>
                                        <p>Sanepa-2, Lalitpur </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="c-social text-center">
                            <ul>
                             <li><a target="_blank" href="#"><i class="fa fa-facebook fa-2x"></i>
                             </a>
                         </li>
                         <li><a target="_blank" href="#"><i class="fa fa-twitter-square fa-2x"></i>
                         </a>
                     </li>
                     <li><a target="_blank" href="#"><i class="fa fa-youtube-square fa-2x"></i>
                     </a>
                 </li>
             </ul>
         </div>
     </div>
 </div>
 <div class="col-lg-8 col-md-6 ">
    <div class="mapouter eqheight">
        <div class="gmap_canvas">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3533.1559507177567!2d85.30026791506154!3d27.681574482802485!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1847f710a121%3A0x8826f2884bb66704!2sModern+Technical+College%2C+Lalitpur!5e0!3m2!1sen!2snp!4v1554013195901!5m2!1sen!2snp" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <style>
        .mapouter {
            position: relative;
            text-align: right;
            height: 400px;
            width: 100%;
            padding: 0;
        }
        .gmap_canvas {
            overflow: hidden;
            background: none !important;
            height: 400px;
            width: 100%;
            }
            </style>
         </div>
     </div>

   </div>
 </div>
</div>

<div class="container-fluid bg-light py-5">
    <div class="row">
        <div class="col-lg-3 col-md-6">
        </div>
        <div class="col-lg-6 col-md-12">
          <div class="contact-form">
            @if ($errors->any())
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @if (Session::has('success'))
            <div class="alert alert-success text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ Session::get('success') }}</p>
            </div>
            @endif
            <form method="post" action="/sendemail/send" role="form">
              {!! csrf_field() !!}
             
              <div class=" container">
                 <div class="row">

                   <div class="form-group col-md-6">
                   
                      <input id="form_name" type="text" name="name" class="form-control" placeholder="Your Name *" required="required" data-error="Name is required.">
                      <div class="help-block with-errors"></div>
                  </div>

                  <div class="form-group col-md-6">
                
                      <input id="form_email" type="email" name="email" class="form-control" placeholder="Your Email *" required="required" data-error="Valid email is required.">
                      <div class="help-block with-errors"></div>
                  </div>

                  <div class="form-group col-md-6">
                     
                      <input id="form_address" type="text" name="address" class="form-control" placeholder="Your Address *" required="required" data-error="Address is required.">
                      <div class="help-block with-errors"></div>
                  </div>

                  <div class="form-group col-md-6">
                     
                      <input id="form_phone" type="text" name="phone" class="form-control" placeholder="Your Mobile *" required="required" data-error="PhoneNumber is required.">
                      <div class="help-block with-errors"></div>
                  </div>

                  <div class="col-md-12">
                   <div class="form-group">
                      
                      <textarea id="form_message" name="message" class="form-control" placeholder="Your Message *" rows="8" required="required" data-error="Please, leave us a message."></textarea>
                      <div class="help-block with-errors"></div>
                  </div>
                 </div>

              <div class="col-md-6">
               <input type="submit" class="btn btn-primary contact_button" value="Send message">
           </div>

       </div>
   </div>
</form>

     </div>                 
    </div>
         <div class="col-lg-3 col-md-6">

         </div>
    </div>
  </div>
</section>




@endsection