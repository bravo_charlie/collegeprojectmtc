@extends('layouts.app')

@section('content')

<section>
	<div class="about-bg">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="about-banner">
						<h2>Testimonials</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="breadcrumb-wrapper">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="/">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Alumni testimonials</li>
				</ol>
			</nav>
		</div>

	</div>
</section>

<section>
	<div class="container">
		<div class="card-columns card-testimonials">
			<div class="card p-1">
				<div class="card-body">
					<p class="card-text">"Proper guidance is the key approach to excellence.
					 Modern Techncial Collge has been that obligatory guidance to me.
					  The support and encouragement of the teachers and friends have 
					  helped me to achieve good knowledge in my Bachelor's due to the 
					  strong background that I set through. 
					  I am heartily thankful to MTC, TU for allowing me to be a part 
					  of the family."</p>
					<div class="alumni-info-wrapper">
						<div class="alumni-img-box">
							<img src="/images/57096920_2309019756041508_9122289337298321408_n-e1564155142866.jpg" alt="">
						</div>
						<div class="alumni-info">
							<div class="name">Sujata Ojha</div>
							<div class="details">BSc Nursing 2016</div>
							<div class="email"><a href="mailto:"></a></div>
						</div>
						<div class="alumni-bio mt-2">Sujata Ojha is pursuing her
						 Master of Nursing in Women Health and Nursing at the 
						 Institute of Medicine, Tribhuvan University.
						  She was the entrance topper of her batch.                               
						   </div>
					</div>

				</div>
			</div>

			<div class="card p-1">
				<div class="card-body">
					<p class="card-text">"It was an excellent opportunity to study M Pharm 
						(Natural Products Chemistry) at Pokhara University. 
						I have gained a tremendous amount of knowledge regarding Natural 
						Product derived Drugs, Spectroscopy, Herbal Pharmacology, 
						Pharmaceutical Chemistry and various other laboratory skills"</p>
					<div class="alumni-info-wrapper">
						<div class="alumni-img-box">
							<img src="/images/16195999_103893393459309_1729309042411918731_n-1-e1564141424836.jpg" alt="">
						</div>
						<div class="alumni-info">
							<div class="name">Ravin Bhandari</div>
							<div class="details">M Pharm 2014</div>
							<div class="email"><a href="mailto:ravinbhandari2000@gmail.com">ravinbhandari2000@gmail.com</a></div>
						</div>
						<div class="alumni-bio mt-2">Ravin Bhandari is currently a faculty
						 member at Crimson College of Technology, Pokhara University. 
						 He has a keen interest in the study of the pharmacological 
						 activity of natural drugs.
						</div>
					</div>

				</div>
			</div>

			<div class="card p-1">
				<div class="card-body">
					<p class="card-text">"Studying in MTC,Tribhuvan University was one of the
					 most remarkable milestones in my academic journey of public health.
					  The curriculum significantly benefited me, by providing a strong
					   knowledge in subject matter through practical sessions and field works.
					    Being able to explore my varied interests with some dedicated and
					     passionate faculty members allowed me to become a well-rounded 
					     professional. Fortunately, the education and field experience 
					     I obtained through BPH at MTC prepared me for my post-graduate 
					     degree as well as my current position as a public health professional.
					     "</p>
					<div class="alumni-info-wrapper">
						<div class="alumni-img-box">
							<img src="/images/12541082_10153410958498590_2846112667773990753_n-e1564138781935.jpg" alt="">
						</div>
						<div class="alumni-info">
							<div class="name">Roshna Rajbhandari</div>
							<div class="details">BPH 2014</div>
							<div class="email"><a href="mailto:roshnarajbhandari@gmail.com">roshnarajbhandari@gmail.com</a></div>
						</div>
						<div class="alumni-bio mt-2">Roshna Rajbhandari is the recipient of prestigious 
						 "Nepal Chatra Bidhya Padak” gold medal for standing faculty top of her batch. 
						 She has been awarded the Asian Development Bank-Japanese Scholarship Programme
						  (ADB-JSP) to study Master of International Public Health at the University of Sydney, 
						  Australia. Currently, she is working as a Project Coordinator-Newborn.
						 </div>
					</div>
				</div>
			</div>

			<div class="card p-1">
				<div class="card-body">
					<p class="card-text">"I was fascinated by the innovative idea on pharmaceutical 
						research especially on Himalayan Drugs at Modern Technical College. 
						The passion, skill and determination nurtured at Tribhuvan University 
						helped me for my higher studies.  
						I am endlessly grateful for the research mentorship and support 
						I received at Pokhara University as it supplemented my academic and 
						research coursework and benefitted both my personal and professional 
						growth as a scientist."</p>
					<div class="alumni-info-wrapper">
						<div class="alumni-img-box">
							<img src="/images/0.jpg" alt="">
						</div>
						<div class="alumni-info">
							<div class="name">Rajendra Karki</div>
							<div class="details">BPharm 2005</div>
							<div class="email"><a href="mailto:rajmankarki@gmail.com">rajmankarki@gmail.com</a></div>
						</div>
						<div class="alumni-bio mt-2">Rajendra Karki obtained Masters (2008) and PhD 
							(2011) from Mokpo National University, Republic of Korea. 
							 He is a Post Doctoral Research Associate at St. Jude Children Research 
							 Hospital, Memphis, US. His research has focused on characterizing NLRs 
							 and inflammasomes as intracellular signaling complexes that orchestrate 
							 innate immune responses.
						</div>
					</div>
				</div>
			</div>

			<div class="card p-1">
				<div class="card-body">
					<p class="card-text">"I can confidently state that joining Modern Technical College
					 was one of my wisest decision so far in my academic career.
					  The enormously competitive curriculum integrated with basic and 
					  clinical research exposures and have made my intake and transition 
					  from undergraduate to PhD way much easier than I had expected.
					   I am greatly indebted to Pokhara University and visionary guidance of Prof.
					    Purusotam Basnet. I will be very fortunate if I can pay back to this 
					    University in any form in the days to come."</p>
					<div class="alumni-info-wrapper">
						<div class="alumni-img-box">
							<img src="/images/NirakarRajbhandari412-e1563527404185.jpg" alt="">
						</div>
						<div class="alumni-info">
							<div class="name">Nirakar Rajbhandari</div>
							<div class="details">BScMLT 2007</div>
							<div class="email"><a href="mailto:nrajbhandari@ucsd.edu">nrajbhandari@ucsd.edu</a></div>
						</div>
						<div class="alumni-bio mt-2">Nirakar Rajbhandari received PhD in 
							Biochemistry and Molecular Biology from University of 
							Nebraska Medical Centre in 2016.  He is currently a 
							postdoctoral scholar at University of California San Diago (UCSD) 
							working on multiple aspects of pancreatic cancer.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



@endsection