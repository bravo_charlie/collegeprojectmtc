 @extends('layouts.app')

 @section('content')
 <section>
    <div class="about-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="about-banner">
                        <h2>Research Management Cell</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/committee">Committee</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Research Management Cell</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<section class="mt-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-desc-title">
                    <h3>Research Management Cell</h3>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="students-desc">
                    <p>
                       Over the past few years, Modern Technical College has been transformed into a major research institution, engaging its faculty members and students in research activities. The faculty members and students in the college are advancing in the field of Laboratory medicine, Public health, and Pharmacy.

The curriculum of Pokhara University is a blend of theoretical and practical exposures in which students acquire adequate knowledge to conduct research for health care.

Both faculty members and students at the college are encouraged to take advantage of a wide range of research opportunities. Each department at the college provides support to students engaged in research and provide orientations on proposal write up, research and articles. The students have the opportunity to work alongside experienced faculty members in a variety of settings.</p>
                      
                   </div>
               </div>
               <div class="col-lg-12">
                <div class="about-desc rmc">
                    
                    <div class="table-responsive-sm">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th>S NO.</th>
                                    <th>Name</th>
                                    <th>Position</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Govinda Akela Paudel</td>
                                    <td> Chairman</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Saroj Kunwar</td>
                                    <td>Secretary</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Rajesh Kumar Thakur</td>
                                    <td>Member</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Ram Krishna Shrestha</td>
                                    <td>Member</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Rupa Nepal</td>
                                    <td>Member</td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Ranjana Chataut</td>
                                    <td>Member</td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>Ronak Shrestha</td>
                                    <td>Member</td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>Amrit Khanal</td>
                                    <td>Member</td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>Sanjita Bista</td>
                                    <td>Member</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            
        </div>
    </div>

</section>

@endsection