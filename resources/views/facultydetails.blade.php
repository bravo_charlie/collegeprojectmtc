@extends('layouts.app')

@section('content')
<section>
        <div class="about-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="about-banner">
                            <h2>Faculty Profile</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="breadcrumb-wrapper">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item " aria-current="page">
                            <a href="#">Faculty
                                Members</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Faculty Profile</li>
                    </ol>
                </nav>
            </div>
        </div>
    </section>
    <section>
            <div class="container mb-4">
                <div class="row ">
            <div class="col-lg-4 col-sm-6">
             <div class="member-profile">
                <img class="card-img-top" src="/uploads/faculty/{{$faculty->image}}" alt="Card image">
            </div>
            </div>
                    <div class="col-lg-8 col-sm-6">
                        <div class="member-profile-details mb-4">
                            <div class="card shadow">
                                <div class="card-body">
                                    <h5 class="card-title">{{$faculty->facultyname}}</h5>
                                    
                                    <p class="profession mb-3">{{$faculty->facultyposition}}</p>
                                    <p><b>Teaching Field: </b> <?php echo ($faculty->teachingfield ); ?> </p>
                                    <p><b>Research Interest: </b><?php echo ($faculty->researchinterest ); ?></p>
                                    <span class="field"><?php echo ($faculty->studysubject) ?></span>
                                    <p><b> Email: </b> <a
                                    href="mailto:#">{{$faculty->email}}</a>
                                                                                
                                    </p>
                                    <ul class="member-social">
                                     <li><a href="#" target="_blank"><i
                                    class="ai ai-researchgate-square ai-3x">
                                </i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="ai ai-google-scholar-square ai-3x">
                                </i>
                            </a>
                        </li>
                                 
                        </ul>
                        <div class="students-desc">
                                        
                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

@endsection