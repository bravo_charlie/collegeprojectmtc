@extends('layouts.app')

@section('content')

<section>
    <div class="about-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="about-banner">
                        <h2>Publication</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Publication</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<section>
	<div class="container">
        <div class="row"> 
            <div class="col-sm-12">
          @foreach($publications as $publication)
			<h4>{{$publication->publication}}</h4>
			<p><?php echo ($publication->researchpaper ) ?></p>
		  @endforeach  
          </div>   
       
       </div>
   </div>
</section>



@endsection