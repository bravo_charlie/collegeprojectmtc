@extends('layouts.app')

@section('content')

<section>
    <div class="about-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="about-banner">
                        <h2>Campus-Life</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">campus-life</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<section class="main-content course-single">
    <div class="container">
        <div class="content-course">
            <div class="row">
                <div class="col-md-12">
                    <article class="post-course">
                        <div class="row">
                            <p>
                            </p>
                            <ul class="wp-block-gallery aligncenter columns-4 is-cropped">
                                @foreach($gallery as $galler)
                                <li class="blocks-gallery-item">
                                    <figure>
                                        <a href="/uploads/gallery/{{$galler->image}}" target="_blank" data-lightbox="pu">
                                            <img src="/uploads/gallery/{{$galler->image}}" alt="" data-id="113" class="wp-image-113"  sizes="(max-width: 1024px) 100vw, 1024px" />
                                        </a>
                                    </figure>
                                </li>
                                @endforeach
                            </ul>

                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>




@endsection