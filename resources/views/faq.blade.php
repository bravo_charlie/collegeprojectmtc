 @extends('layouts.app')

@section('content')

 <section>
        <div class="about-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="about-banner">
                            <h2>FAQ</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="breadcrumb-wrapper">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">FAQ</li>
                    </ol>
                </nav>
            </div>
        </div>
    </section>
    <section class="mt-30">
        <div class="container">
            <div class="row">
                <div class="col-10 mx-auto">
                    <div class="about-desc-title">
                        <h3>General Questions</h3>
                    </div>
                    <div class="accordion" id="faqExample">
                            <div class="card">
                                <div class="card-header p-2" id="heading-775">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#collapse-775" aria-expanded="true"
                                                aria-controls="collapseOne">
                                            Q: Which courses are offered by MTC?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapse-775"
                                     class="collapse show"
                                     aria-labelledby="heading-775"
                                     data-parent="#faqExample">
                                    <div class="card-body">
                                        <p><b>Answer:</b>
                                            <p>Modern Technical College Offers:
BSc.MLT (Bachelor of Science in Medical Laboratory Technology)
B.Pharm Bachelor of Pharmaceutical Sciences
BPH Bachelor of Public Health

                                             </p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header p-2" id="heading-778">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#collapse-778" aria-expanded="true"
                                                aria-controls="collapseOne">
                                            Q: What approvals does MTC have?                                     </button>
                                    </h5>
                                </div>
                                <div id="collapse-778"
                                     class="collapse "
                                     aria-labelledby="heading-778"
                                     data-parent="#faqExample">
                                    <div class="card-body">
                                        <p><b>Answer:</b>
                                            <p>Modern Technical College is affiliated to Pokhara University.</p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                             <div class="card">
                                <div class="card-header p-2" id="heading-779">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#collapse-779" aria-expanded="true"
                                                aria-controls="collapseOne">
                                            Q: What is the Contact Numbers & Email ID for MTC?                                     </button>
                                    </h5>
                                </div>

                                <div id="collapse-779"
                                     class="collapse "
                                     aria-labelledby="heading-779"
                                     data-parent="#faqExample">
                                    <div class="card-body">
                                        <p><b>Answer:</b>
                                            <p>For any Queries you can contact official Telephone No: 01-5555630 or Cell No:9841812980, 9803152684.

</p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header p-2" id="heading-780">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#collapse-780" aria-expanded="true"
                                                aria-controls="collapseOne">
                                            Q: What is the admission procedure of MTC?                                  </button>
                                    </h5>
                                </div>

                                <div id="collapse-780"
                                     class="collapse "
                                     aria-labelledby="heading-780"
                                     data-parent="#faqExample">
                                    <div class="card-body">
                                        <p><b>Answer:</b>
                                            
<img src="/images/faq.jpg">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header p-2" id="heading-780">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#collapse-782" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                What is the duration of each couse and syllabus according to Pokhara University ?                             </button>
                                    </h5>
                                </div>

                                <div id="collapse-782"
                                     class="collapse"
                                     aria-labelledby="heading-782"
                                     data-parent="#faqExample">
                                    <div class="card-body">
                                        <p><b>Answer:</b>
                                            
<img src="/images/course-duration.JPG">
                                        </p>
                                    </div>
                                </div>
                            </div>


                        </div>
                </div>
            </div>
            <!--/row-->
        </div>
        <!--container-->
    </section>
    @endsection