@extends('layouts.dashboard.admin')

@section('content')
<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item">
					<a href="/home/faculty">Faculty</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">create</li>
			</ul>
		</div>
	</div>
	@if($errors->any())
	@foreach($errors->all() as $error)
	<ul>
		<li>{{$error}}</li>
	</ul>
	@endforeach
	@endif
	<div class="card-body card-block">
		<form action="/home/faculty/create" method="POST" class="form-horizontal" enctype="multipart/form-data">
			@csrf
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="name" class=" form-control-label">Faculty Name</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="facultyname" placeholder="Enter Name..." class="form-control">

				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="image" class=" form-control-label">Image input</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="file" id="image" accept="image/png, image/jpg, image/jpeg" name="image" class="form-control-file">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="Faculty Position" class=" form-control-label">Faculty Position</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="facultyposition" placeholder="Enter Teachername..." class="form-control">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="Faculty Position" class=" form-control-label">Priority</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="faculty_priority" placeholder="Enter Priority Number" class="form-control">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="Faculty Position" class=" form-control-label">Teaching Field</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea type="name" id="name" name="teachingfield" placeholder="Enter Teaching Field..." class="form-control ckeditor"></textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="Faculty Position" class=" form-control-label">Research Interest</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea type="name" id="name" name="researchinterest" placeholder="Enter Research Interest..." class="form-control ckeditor"></textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="Faculty Position" class=" form-control-label">Email</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="email" placeholder="Enter email..." class="form-control">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="Faculty Position" class=" form-control-label">Study Subject</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea type="name" id="name" name="studysubject" placeholder="Enter study subject.." class="form-control ckeditor"></textarea>
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-sm">
				<i class="fa fa-dot-circle-o"></i> Submit
			</button>
			<button type="reset" class="btn btn-danger btn-sm">
				<i class="fa fa-ban"></i> Reset
			</button>
		</form>
	</div>
	<div class="card-footer">

	</div>
</div>
@endsection