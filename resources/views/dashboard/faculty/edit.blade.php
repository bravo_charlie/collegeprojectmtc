@extends('layouts.dashboard.admin')

@section('content')
<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item">
					<a href="/home/faculty">Faculty</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">Edit</li>
			</ul>
		</div>
	</div>
	@if($errors->any())
	@foreach($errors->all() as $error)
	<ul>
		<li>{{$error}}</li>
	</ul>
	@endforeach
	@endif
	<div class="card-body card-block">
		<form action="/home/faculty/edit/{{$faculty->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
			@csrf
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="facultyname" class=" form-control-label"> Faculty Name</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="facultyname" value="{{$faculty->facultyname}}" class="form-control">

				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="facultyname" class=" form-control-label"> Faculty Priority</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="faculty_priority" value="{{$faculty->faculty_priority}}" class="form-control">

				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="image" class=" form-control-label">Image input</label>
				</div>
				<div class="col-12 col-md-9 process">
					<input type="file" id="image" accept="image/png, image/jpg, image/jpeg" name="image" class="form-control-file">
					<img src="/uploads/faculty/{{$faculty->image}}" alt="{{$faculty->name}}">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="description" class=" form-control-label">Teacher Name</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="facultyposition" value="{{$faculty->facultyposition}}" class="form-control">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="description" class=" form-control-label">Teaching Field</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea type="name" id="name" name="teachingfield" value="" class="form-control ckeditor">{{$faculty->teachingfield}}</textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="description" class=" form-control-label">Research Interest</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea type="name" id="name" name="researchinterest" value="" class="form-control ckeditor">{{$faculty->researchinterest}}</textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="description" class=" form-control-label">Email</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="email" value="{{$faculty->email}}" class="form-control">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="description" class=" form-control-label">Study Subject</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea type="name" id="name" name="studysubject" value="" class="form-control ckeditor">{{$faculty->studysubject}}</textarea>
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-sm">
				<i class="fa fa-dot-circle-o"></i> Update
			</button>
		</form>
	</div>
	<div class="card-footer">

	</div>
</div>
@endsection