@extends('layouts.dashboard.admin')

@section('content')
<div class="container card">
 <div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">
					<a href="/home/staff">staffs</a>
				</li>
			</ul>
		</div>
<div class="row m-t-30">
	<div class="col-md-12">
	<a style="margin-bottom: 10px;" href="/home/staff/create" class="btn btn-primary">Add New Staff</a>
		<!-- DATA TABLE-->
		<div class="table-responsive m-b-40">
			<table class="table table-borderless table-data3">
				<thead>
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Priority</th>
						<th>Position</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($staff as $staff)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{$staff->name}}</td>
						<td>{{$staff->staff_priority}}</td>
						<td><?php echo ($staff->position ) ?></td>
						<td class="process">
							<img src="/uploads/staff/{{$staff->image}}">
						</td>
							<td style="display:inline-flex;">
								<a href="{{route('staff.edit', $staff->id)}}">
							<button class="btn btn-primary make-btn">Edit</button>
						</a>|
				<form method="post" action="{{route('staff.delete',$staff->id)}}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
				</form>
				</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<!-- END DATA TABLE-->
	</div>
</div>
</div>
</div>

<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
			if(! result){
				evt.stopPropagation();
				evt.preventDefault();	
			}
	}
</script>

@endsection