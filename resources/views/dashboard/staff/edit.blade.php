@extends('layouts.dashboard.admin')

@section('content')
<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item">
					<a href="/home/staff">staffs</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">Edit</li>
			</ul>
		</div>
	</div>
	@if($errors->any())
	@foreach($errors->all() as $error)
	<ul>
		<li>{{$error}}</li>
	</ul>
	@endforeach
	@endif
	<div class="card-body card-block">
		<form action="/home/staff/edit/{{$staff->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
			@csrf
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="name" class=" form-control-label">Name</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="name" value="{{$staff->name}}" class="form-control">
					
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="image" class=" form-control-label">Image input</label>
				</div>
				<div class="col-12 col-md-9 process">
					<input type="file" id="image" accept="image/png, image/jpg, image/jpeg" name="image" class="form-control-file">
					<img src="/uploads/staff/{{$staff->image}}" alt="{{$staff->name}}">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="position" class=" form-control-label">Position</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="position" value="{{$staff->position}}" class="form-control">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="position" class=" form-control-label">Staff Priority</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="text" id="name" name="staff_priority" value="{{$staff->staff_priority}}" class="form-control">
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-sm">
				<i class="fa fa-dot-circle-o"></i> Update
			</button>
		</form>
	</div>
	<div class="card-footer">
		
	</div>
</div>


@endsection