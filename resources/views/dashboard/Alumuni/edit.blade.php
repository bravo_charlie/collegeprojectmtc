@extends('layouts.dashboard.admin')

@section('content')
<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item">
					<a href="/home/alumuni">Almuni</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">Edit</li>
			</ul>
		</div>
	</div>
	@if($errors->any())
	@foreach($errors->all() as $error)
	<ul>
		<li>{{$error}}</li>
	</ul>
	@endforeach
	@endif
	<div class="card-body card-block">
		<form action="/home/alumuni/edit/{{$alumunies->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
			@csrf
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="figurecaption" class=" form-control-label">Figure Caption</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="figurecaption" name="figurecaption" value="{{$alumunies->figurecaption}}" class="form-control">

				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="image" class=" form-control-label">Image input</label>
				</div>
				<div class="col-12 col-md-9 process">
					<input type="file" id="image" accept="image/png, image/jpg, image/jpeg" name="image" class="form-control-file">
					<img src="/uploads/alumuni/{{$alumunies->image}}" alt="{{$alumunies->image}}">
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-sm">
				<i class="fa fa-dot-circle-o"></i> Update
			</button>
		</form>
	</div>
	<div class="card-footer">

	</div>
</div>

@endsection