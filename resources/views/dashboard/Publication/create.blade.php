@extends('layouts.dashboard.admin')

@section('content')
<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item">
					<a href="/home/publication">Publication</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">create</li>
			</ul>
		</div>
	</div>
	@if($errors->any())
	@foreach($errors->all() as $error)
	<ul>
		<li>{{$error}}</li>
	</ul>
	@endforeach
	@endif
	<div class="card-body card-block">
		<form action="/home/publication/create" method="POST" class="form-horizontal" enctype="multipart/form-data">
			@csrf
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="publicationname" class=" form-control-label">Publication Title</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="publicationname" name="publicationname" placeholder="Enter Name..." class="form-control" required>
					
				</div>
			</div>
			
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="publicationpaper" class=" form-control-label"  >Research List</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="publicationpaper" id="publicationpaper" rows="9" class="form-control ckeditor" required></textarea>
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-sm">
				<i class="fa fa-dot-circle-o"></i> Submit
			</button>
			<button type="reset" class="btn btn-danger btn-sm">
				<i class="fa fa-ban"></i> Reset
			</button>
		</form>
	</div>
	<div class="card-footer">
		
	</div>
</div>

@endsection