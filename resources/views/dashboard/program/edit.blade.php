@extends('layouts.dashboard.admin')

@section('content')
<!-- PAGE CONTAINER-->

<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item">
					<a href="/home/programs">programs</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">Edit</li>
			</ul>
		</div>
	</div>
	@if($errors->any())
	@foreach($errors->all() as $error)
	<ul>
		<li>{{$error}}</li>
	</ul>
	@endforeach
	@endif
	<div class="card-body card-block">
		<form action="/home/programs/edit/{{$programs->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
			@csrf
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="programname" class=" form-control-label">Program Name</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="programname" value="{{$programs->programname}}" class="form-control">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="image" class=" form-control-label">Image input</label>
				</div>
				<div class="col-12 col-md-9 process">
					<input type="file" id="image" accept="image/png, image/jpg, image/jpeg" name="image" class="form-control-file">
					<img src="/uploads/programs/{{$programs->image}}" alt="{{$programs->name}}">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="qualification" class=" form-control-label">Qualification</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="qualification" id="description" rows="9"  class="form-control">{{$programs->qualification}}</textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="description" class=" form-control-label">Description</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="description" id="description" rows="9"  class="form-control ckeditor">{{$programs->description}}</textarea>
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-sm">
				<i class="fa fa-dot-circle-o"></i> Update
			</button>
		</form>
	</div>
	<div class="card-footer">

	</div>
</div>


@endsection