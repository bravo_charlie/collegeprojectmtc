
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/images/logo.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Modern Technical College</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
         <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Slider</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/slider"><i class="fa fa-circle-o"></i>Slider</a></li>
            <li><a href="/home/slider/create"><i class="fa fa-circle-o"></i>Create</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Gallery</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/gallery"><i class="fa fa-circle-o"></i>Gallery</a></li>
            <li><a href="/home/gallery/create"><i class="fa fa-circle-o"></i>Create</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Staff</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/staff"><i class="fa fa-circle-o"></i>Staff</a></li>
            <li><a href="/home/staff/create"><i class="fa fa-circle-o"></i>Create</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>News</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/news"><i class="fa fa-circle-o"></i>News</a></li>
            <li><a href="/home/news/create"><i class="fa fa-circle-o"></i> Create</a></li>
           </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Programs</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/programs"><i class="fa fa-circle-o"></i>Program</a></li>
            <li><a href="/home/programs/create"><i class="fa fa-circle-o"></i>Create Program</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Event</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/event"><i class="fa fa-circle-o"></i>Event</a></li>
            <li><a href="/home/event/create"><i class="fa fa-circle-o"></i>Create</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Testimonial</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/testimonials"><i class="fa fa-circle-o"></i>Testimonial</a></li>
            <li><a href="/home/testimonials/create"><i class="fa fa-circle-o"></i>Create Testimonial</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Faculty</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/faculty"><i class="fa fa-circle-o"></i>Faculty</a></li>
            <li><a href="/home/faculty/create"><i class="fa fa-circle-o"></i> Create</a></li>
           </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Alumuni</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/alumuni"><i class="fa fa-circle-o"></i>Alumuni</a></li>
            <li><a href="/home/alumuni/create"><i class="fa fa-circle-o"></i> Create</a></li>
           </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Notice</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/notice"><i class="fa fa-circle-o"></i>Notice</a></li>
            <li><a href="/home/notice/create"><i class="fa fa-circle-o"></i> Create</a></li>
           </ul>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Home Page Popup</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/modal"><i class="fa fa-circle-o"></i>Popup</a></li>
            <li><a href="/home/modal/create"><i class="fa fa-circle-o"></i> Create</a></li>
           </ul>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Publication</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/home/publication"><i class="fa fa-circle-o"></i>Publication</a></li>
            <li><a href="/home/publication/create"><i class="fa fa-circle-o"></i>Create</a></li>
          </ul>
        </li>
        <li class="header">LABELS</li>
          <li>
            <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                  <i class="fa fa-circle-o text-danger"></i>
                  {{ __('Logout') }}
                                  
                  </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
               </form>
              </li>
        </ul>
    </section>
    <!-- /.sidebar -->
