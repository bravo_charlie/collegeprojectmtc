<footer class="site-footer">
    <div class="container">
        <div class="row">
    <div class="col-lg-4 col-md-6">
      <aside id="custom_html-3" class="widget_text widget widget_custom_html">
        <div class="textwidget custom-html-widget">
          <h4 class="footer-heading mb-4 text-white">CONTACT</h4>
				    <div class="footer-contact-info">
            <ul>
              <li>
                <i class='fa fa-phone'></i>
                <div class="info-text">
									<span><a href='tel:+977 9841812980'>+977 9841812980</a></span><br>
									<span><a href='tel:+977 9803152684'>+977 9803152684</a></span>
                </div>
              </li>
               <li>
              <i class="fa fa-map-marker"></i>
                <div class="info-text">
                  <p>Sanepa-2, Lalitpur </p>
                </div>
              </li>
              <li>
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <div class="info-text">
                  <p><a href="#"> mtechcollege@gmail.com</a></p>
                </div>
              </li>
             

            </ul>
          </div>
        </div>
      </aside> 
    </div>
    <div class="col-lg-3 col-md-6">
      <aside id="nav_menu-3" class="widget widget_nav_menu">
        <div class="widget-title">
          <h4>RELATED LINKS</h4>
        </div>
<div class="menu-related-links-container">
<ul id="menu-related-links" class="menu">
<li id="menu-item-198">
  <a href="https://pu.edu.np/" target="_blank">Pokhara University</a>
</li>
<li id="menu-item-689">
  <a href="http://ugcnepal.edu.np/" target="_blank">UGC Nepal</a>
</li>
<li id="menu-item-199">
  <a href="http://moe.gov.np/" target="_blank">MOE GON</a>
</li>
<li id="menu-item-690">
  <a href="http://mohp.gov.np/en/" target="_blank">MOHP GON</a>
</li>
<li id="menu-item-690">
  <a href="https://nhpc.org.np/" target="_blank">NHPC</a>
</li>
<li id="menu-item-690">
  <a href="http://www.nepalpharmacycouncil.org.np/" target="_blank">NPC</a>
</li>
</ul>
</div>
</aside>                
</div>
<div class="col-lg-3 col-md-6">
  <aside id="nav_menu-2" class="widget widget_nav_menu">
  <div class="widget-title">
    <h4>QUICK LINKS</h4>
  </div>
  <div class="menu-header-sub-menu-container">
    <ul id="menu-header-sub-menu-1" class="menu">
      <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4259">
        <a href="#">About</a>
      </li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-150">
  <a href="/campus-life">Gallery</a>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-406">
  <a href="/program">Programs</a>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-31">
  <a rel="noopener noreferrer" href="/faculty">Faculty</a>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-155">
  <a href="/contact">Contact</a>
</li>
</ul>
</div>
</aside> 
 </div>
   <div class="col-lg-2 col-md-6">
     <aside id="custom_html-4" class="widget_text widget widget_custom_html">
        <div class="textwidget custom-html-widget">	
         <h4 class="footer-heading mb-4 text-white">FOLLOW US</h4>
				  <div class="share-social">
					<ul>
						<li>
              <a href="#" target="_blank">
               <i class="fa fa-facebook-square fa-2x"></i>
              </a>
            </li>
						<li>
              <a href="#" target="_blank">
                <i class="fa fa-twitter-square fa-2x"></i>
              </a>
            </li>
						<li>
              <a href="#" target="_blank">
                <i class="fa fa-youtube-square fa-2x"></i>
              </a>
            </li>
					</ul>
				</div>
			 </div>
      </aside>             
    </div>
            
</div>
        <div class="row mt-4">
            <div class="col">
                <div class="copyright text-center">
                    <p class="mt-4">© copyright@ 2020 |
                        Developed By <a href="https://aeydentech.com" target="_blank">Aeyden Technology</a>.</p>
                </div>

            </div>
        </div>
</div>
</footer>