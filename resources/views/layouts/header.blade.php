<header class="header-menu" id="header">
     <div class="top-nav d-none d-lg-block">
        <div class="container">
           <nav class="top-nav-menu navbar navbar-expand-lg topheading ">
              <ul id="menu-header-sub-menu" class="ml-auto">
                <li id="menu-item-4259" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4259 nav-item">
                  <a class="nav-link"  href="/campus-life">Campus Life</a>
                </li>
                <li id="menu-item-150" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-150 nav-item">
                  <a class="nav-link"  href="#">Downloads</a>
                </li>
                <li id="menu-item-406" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-406 nav-item">
                  <a class="nav-link"  href="/faq">FAQ</a>
                </li>
                <li id="menu-item-31" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-31 nav-item">
                  <a class="nav-link"  target="_blank" href="http://mtc.aeydentech.com/webmail">Check Mail</a>
                </li>
                <li id="menu-item-155" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-155 nav-item">
                   <a class="nav-link"  href="/contact">Contact</a>
                </li>
                <li id="menu-item" class="menu-item menu-item-type-custom menu-item-object-custom menu-item nav-item">
                  <a class="nav-link" href="/login">Login</a>
               </li>
             </ul>     
               <form action="#">
                    <label class="expandSearch">
                        <input type="text" placeholder="Search..." name="s">
                        <i class="fa fa-search"></i>
                    </label>
                </form>
           </nav>
        </div>
     </div>
    <div class="container-fluid">
        <div class="nav-wrapper">
            <nav class="navbar navbar-expand-lg navbar-light ">
                <a class="navbar-brand" href="/">
                    <img src="/images/MTC-01.png" alt="image">
                </a>
                <button class="navbar-toggler" type="buttoddn" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

     <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul id="menu-header-menu" class="navbar-nav ml-auto">
            <li id="menu-item-27" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-27 nav-item">
               <a class="nav-link"  href="/">Home</a>
           </li>
        <li id="menu-item-130" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-130 nav-item dropdown">
	      <a class="nav-link dropdown-toggle"  href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About</a>
         <div class="dropdown-menu" role="menu">
            <a class="dropdown-item"  href="/historyofmtc">History Of MTC</a>
            <a class="dropdown-item"  href="/overview">Overview</a>
            <a class="dropdown-item"  href="/organizationalchart">Organizational Structure</a>
            <a class="dropdown-item"  href="/salient-features">Salient Features</a>
            <a class="dropdown-item"  href="/admission">Admission</a>
            <a class="dropdown-item"  href="/internal-examination">Internal Examination</a>
            <a class="dropdown-item"  href="/message-from-chariman">Message from the Chairman</a>
            <a class="dropdown-item"  href="/message-from-principal">Message from the Principal</a>
         </div>
      </li>
      <li id="menu-item-27" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-27 nav-item">
	    <a class="nav-link"  href="/program">Programs</a>
      </li>
      <li id="menu-item-87" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-87 nav-item">
	    <a class="nav-link"  href="/faculty">Faculty</a>
      </li>
      <li id="menu-item-88" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-88 nav-item">
	    <a class="nav-link"  href="/staff">Staff</a>
      </li> 
     
<li id="menu-item-98" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-98 nav-item dropdown">
	<a class="nav-link dropdown-toggle"  href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Research</a>
<div class="dropdown-menu" role="menu">
<a class="dropdown-item"  href="/research">Scientific Publications</a>
<a class="dropdown-item"  href="/committe/rmc">Research Management Cell</a>
</div>
</li>
<li id="menu-item-4260" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4260 nav-item">
	<a class="nav-link" href="#">Journal</a></li>
</ul>
                </div>
            </nav>
        </div>
    </div>
</header>