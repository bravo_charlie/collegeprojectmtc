<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width"/>
     <title>Modern Technical College</title>
     <link rel="canonical" href="#" />
   <meta property="og:locale" content="en_US" />
   <meta property="og:title" content="Home - MTC" />
   <meta property="og:site_name" content="MTC" />
   <meta name="twitter:card" content="summary_large_image" />
   <meta name="twitter:title" content="Home - MTC" />

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('/css/all.css') }}">
     
    <link rel="stylesheet" href="https://cdn.rawgit.com/jpswalsh/academicons/master/css/academicons.min.css">
    <style type="text/css">			
    .heateorSssInstagramBackground
    {background:radial-gradient(circle at 30% 107%,#fdf497 0,#fdf497 5%,#fd5949 45%,#d6249f 60%,#285aeb 90%)}
    .heateor_sss_horizontal_sharing .heateorSssSharing,.heateor_sss_standard_follow_icons_container .heateorSssSharing{
							color: #fff;
						border-width: 0px;
			border-style: solid;
			border-color: transparent;
		}
				.heateor_sss_horizontal_sharing .heateorSssTCBackground{
			color:#666;
		}
				.heateor_sss_horizontal_sharing .heateorSssSharing:hover,.heateor_sss_standard_follow_icons_container .heateorSssSharing:hover{
						border-color: transparent;
		}
		.heateor_sss_vertical_sharing .heateorSssSharing,.heateor_sss_floating_follow_icons_container .heateorSssSharing{
							color: #fff;
						border-width: 0px;
			border-style: solid;
			border-color: transparent;
		}
				.heateor_sss_vertical_sharing .heateorSssTCBackground{
			color:#666;
		}
				.heateor_sss_vertical_sharing .heateorSssSharing:hover,.heateor_sss_floating_follow_icons_container .heateorSssSharing:hover{
						border-color: transparent;
		}
		
		@media screen and (max-width:783px) {
            .heateor_sss_vertical_sharing{
                display:none!important;
                }
                }
        @media screen and (max-width:783px) {
            .heateor_sss_floating_follow_icons_container{
                display:none!important;
                }
                }
    </style>
<!-- This site is optimized with the Yoast SEO plugin v12.1 - https://yoast.com/wordpress/plugins/seo/ -->

<style type="text/css">

img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>

<link rel='stylesheet' id='wp-block-library-css'  href="{{ asset('/css/style.min.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href="{{ asset('/css/styles.css?ver=5.1.4') }}" type='text/css' media='all' />
<!-- <link rel='stylesheet' id='bootstrap.css-css'  href="{{ asset('/css/bootstrap.min.css?ver=5.3') }}"  type='text/css' media='all' /> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!-- <link rel='stylesheet' id='lightbox.css-css'  href="{{ asset('/css/lightbox.min.css?ver=5.3') }}" type='text/css' media='all' /> -->
<link rel='stylesheet' id='slick.css-css'  href="{{ asset('/css/slick.css?ver=5.3') }}" type='text/css' media='all' />
 <link rel='stylesheet' id='slick-theme.css-css'  href="{{ asset('/css/slick-theme.css?ver=5.3') }}" type='text/css' media='all' />
 <!-- <link rel='stylesheet' id='toastr.min.css-css'  href="{{ asset('/css/toastr.min.css?ver=5.3') }}" type='text/css' media='all' /> -->
<link rel='stylesheet' id='bootstrap-datepicker3.min.css-css'  href="{{ asset('/css/bootstrap-datepicker3.min.css?ver=5.3') }}" type='text/css' media='all' />
<link rel='stylesheet' id='nepali.datepicker.v2.2.min.css-css'  href="{{ asset('/css/nepali.datepicker.v2.2.min.css?ver=5.3') }}" type='text/css' media='all' />
<link rel='stylesheet' id='custom.css-css'  href="{{ asset('/css/custom.css?ver=5.3') }}" type='text/css' media='all' />
<link rel='stylesheet' id='style.css-css'  href="{{ asset('/css/style.css?ver=5.3') }}" type='text/css' media='all' />
<link rel='stylesheet' id='heateor_sss_frontend_css-css'  href="{{ asset('/css/sassy-social-share-public.css?ver=3.3.8') }}" type='text/css' media='all' />
<link rel='stylesheet' id='heateor_sss_sharing_default_svg-css'  href="{{ asset('/css/sassy-social-share-svg.css?ver=3.3.8') }}" type='text/css' media='all' />
<script type='text/javascript' src="{{ asset('/jss/jquery.js?ver=1.12.4-wp') }}"></script>
<!-- <script type='text/javascript' src="{{ asset('/jss/jquery-migrate.min.js?ver=1.4.1') }}"></script> -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145169456-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145169456-1');
</script>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    @include('layouts.header')
            @yield('content')
    @include('layouts.footer')

</body>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/pushas.edu.np\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src="{{ asset('/jss/scripts.js?ver=5.1.4') }}"></script>

<!-- <script type='text/javascript' src="{{ asset('/jss/bootstrap.min.js?ver=1') }}"></script> -->
<!-- <script type='text/javascript' src="{{ asset('/jss/lightbox.min.js?ver=1') }}"></script>
 --><script type='text/javascript' src="{{ asset('/jss/jquery.min.js?ver=1') }}"></script>
<script type='text/javascript' src="{{ asset('/jss/slick.js?ver=1') }}"></script>
<!-- <script type='text/javascript' src="{{ asset('/jss/toastr.min.js?ver=1') }}"></script>
 --><script type='text/javascript' src="{{ asset('/jss/jquery.matchHeight.js?ver=1') }}"></script>
<script type='text/javascript' src="{{ asset('/jss/bootstrap-datepicker.min.js?ver=1') }}"></script>
<script type='text/javascript' src="{{ asset('/jss/nepali.datepicker.v2.2.min.js?ver=1') }}"></script>
<script type='text/javascript' src="{{ asset('/jss/theme.js?ver=1') }}"></script>
<script type='text/javascript'>
function heateorSssLoadEvent(e) {var t=window.onload;if (typeof window.onload!="function") {window.onload=e}else{window.onload=function() {t();e()}}};	var heateorSssSharingAjaxUrl = 'https://pushas.edu.np/wp-admin/admin-ajax.php', heateorSssCloseIconPath = 'https://pushas.edu.np/wp-content/plugins/sassy-social-share/public/../images/close.png', heateorSssPluginIconPath = 'https://pushas.edu.np/wp-content/plugins/sassy-social-share/public/../images/logo.png', heateorSssHorizontalSharingCountEnable = 0, heateorSssVerticalSharingCountEnable = 0, heateorSssSharingOffset = -10; var heateorSssMobileStickySharingEnabled = 0;var heateorSssCopyLinkMessage = "Link copied.";var heateorSssUrlCountFetched = [], heateorSssSharesText = 'Shares', heateorSssShareText = 'Share';function heateorSssPopup(e) {window.open(e,"popUpWindow","height=400,width=600,left=400,top=100,resizable,scrollbars,toolbar=0,personalbar=0,menubar=no,location=no,directories=no,status")};var heateorSssWhatsappShareAPI = "web";
</script>
<script type='text/javascript' src="{{ asset('/jss/sassy-social-share-public.js?ver=3.3.8') }}"></script>
<script type='text/javascript' src="{{ asset('/jss/wp-embed.min.js?ver=5.3') }}"></script>
<script src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>

<script>
    // Add the following code if you want the name of the file appear on select
    jQuery(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        jQuery(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>

<script>
    jQuery(function () {
        jQuery('.eqheight').matchHeight();
    });
</script>

<script>
    jQuery(function () {
        jQuery('.card-equal').matchHeight();
        jQuery('#nav_menu-3 .menu-item a').attr('target', '_blank');
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.programs-shas').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });
</script>


<script type="text/javascript">
 $(window).on("load",function(){
    jQuery('#NoticeModal').modal('show');
});
</script>

