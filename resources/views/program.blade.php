@extends('layouts.app')

@section('content')
<section>
        <div class="about-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="about-banner">
                            <h2>Programs</h2>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="breadcrumb-wrapper">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Programs</li>
                    </ol>
                </nav>
            </div>

        </div>
    </section>


   <section class="mt-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-tittle ">
                        <h2>Programs</h2>
                    </div>
                </div>
            </div>
            <div class="row programs-list">
                 @foreach($programs as $program)
                        <div class="col-lg-4" style="margin-top:10px;">
                         <div class="single-block">
                            <?php 
                        $string = $program->image;
                        $string = substr(strrchr($string, '.'), 1);
                        ?>
                        @if($string == "jpg" || $string == "png" || $string == "jpeg")
                        <a href="{{$program->id}}">
                        <img width="554" height="654" src="../uploads/programs/{{$program->image}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" sizes="(max-width: 554px) 100vw, 554px" />
                        </a>
                        @endif
                        <div class="g-overlay"></div>
                     <div class="programme-name">
                        <div class="cs-post-header">
                         <div class="cs-category-links">
                             <a class="overlay-top" href="/programdetails/{{$program->id}}">
                                 <h4>{{$program->programname}}</h4>
                                 <span class="price yellow"><?php echo ($program->qualification ) ?></span>
                             </a>

                         </div>                                
                     </div> 
                     </div>
                    </div>
                    </div>
                    @endforeach
             </div>
         </div>
    </section>




@endsection