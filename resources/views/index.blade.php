@extends('layouts.app')

@section('content')


 @if(count($modals)>=1)
<div class="modal fade" id="NoticeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <?php $count=1; ?>
    @foreach($modals as $m)
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">{{$m->modalname}} </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p>
                    </p><center><img class="img-responsive" style="width:100%;height:auto;" src="{{ asset('/uploads/modal/'.$m->image )}}"></center>
                <p></p> 
                <p class="text-center">
                    </p><p style="text-align: justify;"><br></p>

                <p></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal" style="background:red;color:#fff">Close</button>
      
      </div>
    </div>
    <?php $count ++;
    if($count > 1)
        break;
    ?>
      @endforeach
   </div>
</div>
@endif

<section>
  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <!-- <ol class="carousel-indicators">
         @foreach($sliders as $key => $slider)
      <li data-target="#myCarousel" data-slide-to="{{$slider->id}}" class="{{ $key == 0 ? ' active' : '' }}"></li>
       @endforeach
     </ol> -->
     <div class="carousel-inner">
       @foreach($sliders as $key => $slider)
       <div class="carousel-item {{ $key == 0 ? ' active' : '' }}">
        <img class="d-block w-100" src="/uploads/{{$slider->image}}">
      </div>
      @endforeach
    </div>
    
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</section>
<section class="notice-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-12">
        <div class="notice-card eqheight">
          <div class="notice-heading">
            <h3>Notice</h3>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-6">
                <div class="notice-content">
                  <!-- single notice -->
                  <?php $count=1;?>
                  @foreach ($notices as $notice)
                  @if($count%2 == 0)
                  <!-- single notice -->       <div class="single-notice">
                  <h6><a href="/noticedetails/{{$notice->id}}">{{$notice->noticetitle}}</a>
                  </h6>
                  <p class="notice-date">{{$notice->created_at->format('M d , Y')}}</p>
                </div>
                @endif
                <?php $count++; ?>
                <?php if($count>12)
                break
                ?>
                @endforeach 

              </div>
            </div> 
            <div class='col-lg-6'>
             <div class='notice-content'>
              <?php $count=1;?>
              @foreach ($notices as $notice)
              @if($count%2 != 0)
              <div class="single-notice">
                <h6><a href="/noticedetails/{{$notice->id}}">{{$notice->noticetitle}}</a>
                </h6>
                <p class="notice-date">{{$notice->created_at->format('M d , Y')}}</p>
              </div>
              @endif
              <?php $count++; ?>
              <?php if($count>12)
              break
              ?>
              @endforeach                          
            </div>
          </div> 
        </div>
      </div>

      <div class="v-more">
        <button type="button" class="btn btn-link ">
          <a href="/notice"> View More 

            <i class="fa fa-angle-double-right"></i>
          </a>
        </button>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-md-4 col-sm-12">
    <div class="news-card eqheight">
      <div class="news-heading">
        <h3>News</h3>
      </div>
      <div class="container12">
        <div class="row">
          <div class="col-lg-12">
            <div class="notice-content">
             <!-- single news -->
             <?php $count=1;?>
             @foreach($news as $news)
             <div class="single-news">
              <?php 
              $string = $news->image;
              $string = substr(strrchr($string, '.'), 1);
              ?>
              @if($string == "jpg" || $string == "png" || $string == "jpeg")
              <div class="post-img">
                <img width="55" height="80" src="/uploads/news/{{$news->image}}"  alt="" sizes="(max-width: 55px) 100vw, 55px" />
              </div>
              @endif
              <div class="post-info">
                <h6>
                 <a href="/newsdetails/{{$news->id}}">{{$news->name}}</a>
               </h6>
               <p class="news-date">{{$news->created_at->format('M d , Y')}}</p>
             </div>

             <?php $count++; ?>
             <?php if($count>5)
             break
             ?>
           </div>
           @endforeach
         </div>
       </div>
     </div>
     <div class="v-more">
       <button type="button" class="btn btn-link ">
         <a  href="/news">
           View More
           <i class="fa fa-angle-double-right">
           </i>
         </a>
       </button>
     </div>
   </div>
 </div>
</div>
</div>
</section>
<section class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section-tittle ">
          <h2>Programs</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="programs-shas">
          @foreach($programs as $program)
          <div class="single-block">
            <?php 
            $string = $program->image;
            $string = substr(strrchr($string, '.'), 1);
            ?>
            @if($string == "jpg" || $string == "png" || $string == "jpeg")
            <a href="#">
              <img width="554" height="654" src="/uploads/programs/{{$program->image}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" sizes="(max-width: 554px) 100vw, 554px" />
            </a>
            @endif
            <div class="g-overlay"></div>
            <div class="programme-name">
              <div class="cs-post-header">
                <div class="cs-category-links">
                  <a class="overlay-top" href="/programdetails/{{$program->id}}">
                    <h4>{{$program->programname}}</h4>
                    <span class="price yellow"><?php echo ($program->qualification ) ?></span>
                  </a>
                </div>                                
              </div> 
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>

<section class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section-tittle ">
          <h2>Events</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <?php $count=1;?>
      @foreach($events as $event)
      <div class="col-lg-4 col-sm-4 card-equal">
       <div class="event-content">
        <?php 
        $string = $event->image;
        $string = substr(strrchr($string, '.'), 1);
        ?>
        @if($string == "jpg" || $string == "png" || $string == "jpeg")
        <div class="card eqheight">
          <a href="/eventdetails/{{$event->id}}">
            <img class="card-img-top"
            src="/uploads/events/{{$event->image}}"
            alt="Card image cap">
          </a>
          @endif
          <div class="card-body">
           <p class="event-date">
             <span><i class="fa fa-calendar-alt"></i>{{$event->created_at->format('Y , M d ')}}</span>
             <span><i class="fa fa-clock"></i>{{$event->created_at->format('h:i:sa')}}</span></p>
             <h5 class="card-title">
               <a href="/eventdetails/{{$event->id}}">{{$event->title}}</a></h5>

               <p class="card-text">
                <?php 
                $excerpt = $event->description;
                $the_str = substr($excerpt, 0, 100);
                echo $the_str; 
                ?>
              </p> 
            </div>

          </div>
        </div>
        <?php $count++; ?>
        <?php if($count>3)
        break
        ?>
      </div>
      @endforeach
    </div>
    <div class="row">
      <div class="col-12">
        <div class="v-more">
          <button type="button" class="btn btn-link "><a href="/news">View More<i
            class="fa fa-angle-double-right"></i></a>
          </button>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="fun-facts">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-6">
          <!-- Single Fact -->
          <div class="single-fact">
            <img src="/images/shas.png"
            alt="">
            
            <div class="number"><span class="counter">2010</span>
            </div>
            <p>Established </p>
          </div>
          
        </div>
        <div class="col-lg-3 col-md-6 col-6">
          
          <div class="single-fact">
            <img src="/images/books.png"
            alt="">
            <div class="number"><span class="counter">{{$programCount}}</span>
            </div>
            <p>Programs</p>
          </div>
          
        </div>

        <div class="col-lg-3 col-md-6 col-6">
         <div class="single-fact">
          <img src="/images/student.png"
          alt="">
          <div class="number"><span class="counter">500+</span>
          </div>
          <p>Students</p>
        </div>
        
      </div>
      <div class="col-lg-3 col-md-6 col-6">
        <div class="single-fact">
          <img src="/images/d.png"
          alt="">
          <div class="number"><span class="counter">50+</span>
          </div>
          <p>Scholarships</p>
        </div>
        
      </div>
    </div>
  </div>
</div>
</section>
<section class="gallery-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section-tittle ">
          <h2>Gallery</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <?php $count=1;?>
      @foreach($gallery as $galler)
      <div class="col-md-6 col-lg-3 no-gutters">
        <div class="gallery-img-div">
          <?php 
          $string = $galler->image;
          $string = substr(strrchr($string, '.'), 1);
          ?>
          @if($string == "jpg" || $string == "png" || $string == "jpeg")
          <a href="/uploads/gallery/{{$galler->image}}" data-lightbox="pu" target="_blank">
            <img src="/uploads/gallery/{{$galler->image}}" alt="" data-id="120"  class="gallery-img" sizes="(max-width: 1024px) 100vw, 1024px" />
          </a>
          @endif
        </div>
        <?php $count++; ?>
        <?php if($count>4)
        break
        ?>
      </div>
      @endforeach
    </div>
  </div>
</section>
<section id="contact-form-side">
  <div class="container" style="margin-bottom:15px;">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="container">
         <div class="row">
           <div class="col-lg-12">
            <div class="section-tittle ">
              <h2>Find Us On Map</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="mapouter eqheight">
        <div class="gmap_canvas">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3533.1559507177567!2d85.30026791506154!3d27.681574482802485!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1847f710a121%3A0x8826f2884bb66704!2sModern+Technical+College%2C+Lalitpur!5e0!3m2!1sen!2snp!4v1554013195901!5m2!1sen!2snp" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <style>
        .mapouter {
         position: relative;
         text-align: right;
         height: 400px;
         width: 100%;
         padding: 0;
       }
       .gmap_canvas {
         overflow: hidden;
         background: none !important;
         height: 400px;
         width: 100%;
       }
       </style>
     </div>
   </div>
   <div class="col-lg-6 col-md-6 col-sm-12">
    <div class="container12">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-tittle ">
            <h2>Contact Us</h2>
          </div>
        </div>
      </div>
    </div>
    <div id="contact" class="contact-form">
      @if ($errors->any())
      <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      
      <div class="hideme alert alert-success text-center" id="hideme" >
        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
        <p style="border-bottom:0px;">Message Sent Successfully</p>
      </div>
      
      <form role="form"  id="contactForm">
       @csrf
       <input type ="hidden" name="subject" value="inquiry" id="subject">
       <div class=" container12">
        <div class="row">
          
          <div class="form-group col-md-6">
           
            <input id="form_name" type="text" name="name" class="form-control" placeholder="Your Name *" required="required" data-error="Name is required.">
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group col-md-6">
           
            <input id="form_email" type="email" name="email" class="form-control" placeholder="Your Email *" required="required" data-error="Valid email is required.">
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group col-md-6">
           
            <input id="form_address" type="text" name="address" class="form-control" placeholder="Your Address *" required="required" data-error="Address is required.">
            <div class="help-block with-errors"></div>
          </div>

          <div class="form-group col-md-6">
            
            <input id="form_phone" type="text" name="phone" class="form-control" placeholder="Your Mobile *" required="required" data-error="PhoneNumber is required.">
            <div class="help-block with-errors"></div>
          </div>
          
          <div class="col-md-12">
            <div class="form-group">
              
              <textarea id="form_message" name="message" class="form-control" placeholder="Your Message *" rows="8" required="required" data-error="Please, leave us a message."></textarea>
              <div class="help-block with-errors"></div>
            </div>
          </div>

          <div class="col-md-6">
            <input type="submit" class="btn btn-primary contact_button" onclick="callAjax(event)" value="Send message">
          </div>

        </div>
      </div>
    </form>
    
    <script type="text/javascript">
    function callAjax(evt){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: '/sendemail/sendmail',
        type: 'post',
        data: {
          subject: $('#subject').val(),
          name: $('#form_name').val(),
          email: $('#form_email').val(),
          address: $('#form_address').val(),
          phone: $('#form_phone').val(),
          message: $('#form_message').val()
        },
        success: function(result){
          console.log('anil',result);
          var element = document.getElementById("hideme");
          element.classList.add("showme");

        },
        error: function (data, textStatus, errorThrown) {
          console.log(data);
        },
      });
      $("#contactForm").trigger("reset");
      evt.preventDefault();
    }
    </script>
  </div>                 
</div>
</div>
</div>
</section>


@endsection