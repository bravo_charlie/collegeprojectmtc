@extends('layouts.app')

@section('content')

<section>
                <div class="about-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="about-banner">
                                    <h2>Salient Features</h2>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="breadcrumb-wrapper">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active"
                                    aria-current="page">Salient Features</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </section>
            
            <section class="mt-30">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="students-desc-title">
                                <h3>Salient Features</h3>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="students-desc">
                                
<figure class="wp-block-image">
<ol>
<li>9 years of excellence in Bsc. MLT programme</li>
<li>Semester based international curriculam.</li>
<li>State of art own 100 bed Star Hospital.</li>
<li>Separate basic and clinical laboratary.</li>
<li>AC class rooms, multimedia.</li>
<li>Separate boys & girls hostel.</li>
<li>Laboratary posting to reputed hospitals.</li>
<li>Problem based learning.</li>
<li>Research and publication oppurtunities.</li>
<li>Highly professionals, experienced full time faculties.</li>
<li>Very easily accessible location(Sanepa ring road near Balkhu)</li>
<li>High speed internet access in the computer room</li>
<li>Group discussion and interaction</li>
<li>Directed study and seminars</li>
<li>Routine and special diagnostic laboratary bench work practices</li>
</ol>
</figure>




      </div>
         </div>
                  </div>
                </div>
            </section>




  @endsection