@extends('layouts.app')

@section('content')
<section>
    <div class="about-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="about-banner">
                        <h2>Staff</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Staff Members</li>
                </ol>
            </nav>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row"> 
            @foreach($staffs as $staff)
            <div class="col-lg-4 col-sm-6">
                <div class="single-member card-deck eqheight">
                    <div class="card shadow">
                        <img class="card-img-top" src="/uploads/staff/{{$staff->image}}" alt="Bishnu Raj Tiwari" sizes="(max-width: 360px) 100vw, 360px" />
                        <div class="card-body">
                          <h4 class="card-title">{{$staff->name}}</h4>
                          <p><?php echo ($staff->position ) ?></p>
                      </div>   
                  </div>
              </div>
          </div>
          @endforeach
      </div>
      <div class="row page_nav_row">
        <div class="col">
            <div class="page_nav">
                <ul class="d-flex flex-row align-items-center justify-content-center">
                    <li style="list-style:none;"> {{ $staffs->links() }}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
</section>


@endsection
