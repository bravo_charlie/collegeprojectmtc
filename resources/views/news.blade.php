@extends('layouts.app')

@section('content')
<section>
    <div class="about-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="about-banner">
                        <h2>News</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="breadcrumb-wrapper">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">News</li>
                </ol>
            </nav>
        </div>

    </div>
</section> 
<section class="notice-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="notice-card eqheight">
                    <div class="notice-heading">
                        <h3>News</h3>
                    </div>
                    <div class="container">

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="notice-content">

                                  <!-- single notice -->
                                  <?php $count=0;?>
                                  @foreach ($news as $new)
                                  @if($count%2 == 0)
                                  <div class="single-notice">
                                    <h6><a href="/newsdetails/{{$new->id}}">{{$new->name}}</a>
                                      </h6>
                                      <p class="notice-date">{{$new->created_at->format('M d , Y')}}</p>
                                  </div>
                                  @endif
                                  <?php $count++; ?>
                                  <?php if($count>12)
                                  break
                                  ?>
                                  @endforeach 
                              </div>                                   
                          </div>
                      
                      <div class='col-lg-6'> 
                       <div class='notice-content'>
                          <?php $count=0;?>
                          @foreach ($news as $new)
                          @if($count%2 != 0)
                          <div class="single-notice">
                              <h6><a href="/newsdetails/{{$new->id}}">{{$new->name}}</a>
                              </h6>
                              <p class="notice-date">{{$new->created_at->format('M d , Y')}}</p>
                          </div>
                          @endif
                          <?php $count++; ?>
                          <?php if($count>12)
                          break
                          ?>
                          @endforeach                          
                      </div>
                  </div>
              </div>
          </div>
      </div>
         </div>
   </div>
  </div>
</section>
@endsection