@extends('layouts.app')

@section('content')
<div id="content" role="main" class="span8 offset2">

    <section>
        <div class="about-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="about-banner">
                            <h2>Overview</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="breadcrumb-wrapper">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active"
                        aria-current="page">Overview</li>
                    </ol>
                </nav>
            </div>
        </div>
    </section>

    <section class="mt-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="students-desc-title">
                        <h3>Overview</h3>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="students-desc">
                        <p>Modern Technical College(MTC) is one of the growing institutions 
                            for producing level human resource of Allied health sciences under the
                            umbrella of Star Hospital. </p>
                            <p>MTC is affiliated to Pokhara University
                               and managed by academicians,researchers,industrialists, entrepreneurs and 
                               prominent professional with extensive exposure to a wide range of knowledge,
                               experience and teaching/learning practices. The institution is commited to 
                               provide quality,practical and real life based education and aims to be
                               A CENTRE OF EXCELLENCE through quality education, research and services. 
                               Four years bachelor or Medical</p>
                               <p>Laboratary Science program(BSc MLT) has been running at MTC since 2010.
                                   We are proud to announce that our 1st, 2nd and 3rd batch students have recently graduated.
                                   We are devoted to produce skillful, market oriented and competent graduate who can 
                                   contribute in delivering the diagnostic services efectively.
                               </p>
                                <div class="students-desc-title">
                                    <h3>Vision</h3>
                                </div>
                                <p> By the year 2022,Modern Technical College aims to be a leading Centre of Excellence
                                  in Medical Education and Research. Students graduating from the institute will have
                                  all the required skills to deliver quality health care to all sections of the society
                                  with compassion and benevolence, without prejudice or descrimination, at affordable 
                                  cost. As a Research Centre,it shall focus on finding bette, safer and affordable 
                                  ways of diagnosing, treating and preventing disease, In doing so, it will maintain 
                                  highest ethical standards.
                                </p>
                                  <div class="students-desc-title">
                                    <h3>Mission</h3>
                                </div>
                                <p> To provide the world class education by good academic management,
                                 research activities, and evidence based practices. Extensive practical 
                                 exposure opportunities to gain adequate skill by using cutting age scientific
                                 approaches. To provide technically sound and academically excellent allied 
                                 health professionals to the health care market who are well competent to deliver
                                 quality health services. 
                                </p>
                             <div class="students-desc-title">
                                <h3>Goal and Objectives</h3>
                            </div>
                            <ol>
                                <li>Unveil the strength of learners through the extension of programs</li>
                                <li>Develop highly equipped labs to enhance education and experiment</li>
                                <li>Prepare creative and research-oriented scientists in the field of basic, as well as applied sciences, Produce well qualified and skilled graduates to fulfill the national and international demand</li>
                            </ol>
                            <div class="students-desc-title">
                                <h3>Salient Features</h3>
                            </div>
                            <ol>
                                <li>9 years of excellence in Bsc. MLT programme</li>
                                <li>Semester based international curriculam.</li>
                                <li>State of art own 100 bed Star Hospital.</li>
                                <li>Separate basic and clinical laboratary.</li>
                                <li>AC class rooms, multimedia.</li>
                                <li>Separate boys & girls hostel.</li>
                                <li>Laboratary posting to reputed hospitals.</li>
                                <li>Problem based learning.</li>
                                <li>Research and publication oppurtunities.</li>
                                <li>Highly professionals, experienced full time faculties.</li>
                                <li>Very easily accessible location(Sanepa ring road near Balkhu)</li>
                                <li>High speed internet access in the computer room</li>
                                <li>Group discussion and interaction</li>
                                <li>Directed study and seminars</li>
                                <li>Routine and special diagnostic laboratary bench work practices</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>
     </div>
    @endsection