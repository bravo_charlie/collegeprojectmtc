@extends('layouts.app')

@section('content')
<section>
                <div class="about-bg">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="about-banner">
                                    <h2>Message from the Principal</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="breadcrumb-wrapper">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active"
                                    aria-current="page">Message from the Principal</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </section>

             <section class="mt-30">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="students-desc-title">
                                <h3>Message from the Principal</h3>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="students-desc">
                              <div class="wp-block-image">
	                            <figure class="alignleft is-resized">
		                          <img src="/images/govinda1.jpg" alt="Basanta" class="wp-image-210" width="499" height="330"/>
		                            <figcaption style="color:#f37021;"><strong>Mr. Govinda Akela Paudel<br>Principal</strong></figcaption>
	                            </figure>
                             </div>
      <p>         
      Modern Technical college, Sanepa, has been in the forefront of providing 
      professional Health Science education in the nation for the last 10 years.  
      Recognized for quality education, the college has been always first choice of
      students. With dedicated and skilled faculty, excellent infrastructure, library 
      facilities, laboratories and skill labs, that provide practical experience to the 
      students, the college has been attracting numerous students from every corner of 
      the Nepal.  The students who have graduated from this college have been
      successfully practicing and perusing their master degree in motherland as well as
      abroad. 
     </p>
     <p>
      Our effort has always been to provide the best to the students and to prepare them 
     to take up the challenges of tomorrow so that when they come out of the college, they 
     are competent to handle the responsibilities of the health science profession. Students 
     get opportunities to interact with students of other professional disciplines and gain 
     knowledge about different cultures, facilitating their overall development.
     </p>
    <p>
      The attached tertiary care hospital, Star Hospital, Sanepa with excellent patient care 
    facilities, provides the students good opportunities to learn bedside manners and 
    clinical skills.  The rural community exposure provides primary care facilities to the 
    rural population and also provides an inside view of the rural health care management 
    for the health science students. Our own group also has 2 pharmaceuticals industries 
    where students can get maximum exposure to industry.
    </p>
    <p>
      The institution attracts research grants from university and other
      funding agencies and provides an opportunity for the faculty and students to sharpen 
      their research skills.
    </p>
    <p>
      True to its mission, at MTC,Sanepa, it has been our constant effort to train 
      competent, compassionate and caring health professionals through excellence in 
      teaching, patient care, and medical research.
    </p>
    <p>Words can neither qualify nor quantify how helpful is our college management board.
     Under the leadership of our Dynamic Chairman Mr. Kishore Kumar Maharjan, I sense 
     exuberance every single day.
    </p>
    <p>
      I express my gratitude to all the stakeholders and concerned people and would like
     to assure that I will leave no stones unturned to take this institution to newer 
     heights by enhancing academics that would help advance quality education. Let us 
     work together on our common dreams. Exuberance
    </p>
         </div>
       </div>
    </div>
  </div>
</section>
@endsection