<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $events = Event::all();
        return view ('dashboard.event.index',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view ('dashboard.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $events = new Event();
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $events->title = $request->title;
        $events->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "events".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/events');
            $request->file('image')->move($location, $image);
            $events->image = $image;
        }
        else{
            $events->image = 'default-thumbnail.png';
        }        
        $events->save();
        return redirect('/home/event');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event,$id)
    {
         $events = Event::findOrFail($id);
        return view ('dashboard.event.edit',compact('events'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event,$id)
    {
        $events = Event::findOrFail($id);
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $events->title = $request->title;
        $events->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "events".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/events');
            $request->file('image')->move($location, $image);
            $events->image = $image;
        }
        else{
            $events->image = $events->image;
        }        
        $events->save();
        return redirect('/home/event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event,$id)
    {
        $events = Event::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
