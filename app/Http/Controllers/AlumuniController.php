<?php

namespace App\Http\Controllers;

use App\Alumuni;
use Illuminate\Http\Request;

class AlumuniController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $alumunies = Alumuni::all();
        return view ('dashboard.Alumuni.index',compact('alumunies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.Alumuni.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alumunies = new Alumuni();
        $request->validate([
           
            'figurecaption' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $alumunies->figurecaption = $request->figurecaption;
        if(file_exists($request->file('image'))){
            $image = "alumunies".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/alumuni');
            $request->file('image')->move($location, $image);
            $alumunies->image = $image;
        }
        else{
            $alumunies->image = 'default-thumbnail.png';
        }        
        $alumunies->save();
        return redirect('/home/alumuni');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alumuni  $alumuni
     * @return \Illuminate\Http\Response
     */
    public function show(Alumuni $alumuni,Request $request,$id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alumuni  $alumuni
     * @return \Illuminate\Http\Response
     */
    public function edit(Alumuni $alumuni, $id)
    {
          $alumunies = Alumuni::findOrFail($id);
        return view ('dashboard.alumuni.edit',compact('alumunies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alumuni  $alumuni
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Alumuni $alumuni,$id)
    {
        $alumunies = Alumuni::findOrFail($id);
        $alumunies->figurecaption = $request->figurecaption;
        if(file_exists($request->file('image'))){
            $image = "alumunies".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/alumuni');
            $request->file('image')->move($location, $image);
            $alumunies->image = $image;
        }
        else{
            $alumunies->image = $alumunies->image;
        } 
        $alumunies->save();  
        return redirect('/home/alumuni');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alumuni  $alumuni
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alumuni $alumuni)
    {
        $alumunies = Alumuni::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
