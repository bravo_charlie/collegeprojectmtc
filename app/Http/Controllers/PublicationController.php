<?php

namespace App\Http\Controllers;

use App\publication;
use Illuminate\Http\Request;

class PublicationController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $publications = publication::all();
         return view ('dashboard.Publication.index',compact('publications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
    {
        return view ('dashboard.Publication.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $publications = new publication();
   $request->validate([
            'publicationname' => 'required',
           'publicationpaper' => 'required',
                    ]);
      $publications->publication = $request->publicationname;
      $publications->researchpaper = $request->publicationpaper;
      $publications->save();

     return redirect('/home/publication');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function show(publication $publication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function edit(publication $publication,$id)
    {
        $publications = publication::findOrFail($id);
        return view ('dashboard.Publication.edit',compact('publications'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, publication $publication,$id)
    {
         $publications = publication::findOrFail($id);
           $request->validate([
            'publicationname' => 'required',
           'publicationpaper' => 'required',
                    ]);
      $publications->publication = $request->publicationname;
      $publications->researchpaper = $request->publicationpaper;
      $publications->save();

     return redirect('/home/publication');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function destroy(publication $publication)
    {

        $publications = publication::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
