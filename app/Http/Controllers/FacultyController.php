<?php

namespace App\Http\Controllers;

use App\Faculty;
use Illuminate\Http\Request;

class FacultyController extends Controller
{


     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faculty = Faculty::all();
        return view ('dashboard.faculty.index',compact('faculty'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.faculty.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $faculty = new Faculty();
        $request->validate([
            'facultyname' => 'required',
            'facultyposition' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $faculty->facultyname = $request->facultyname;
        $faculty->faculty_priority = $request->faculty_priority;
        $faculty->facultyposition = $request->facultyposition;
        $faculty->teachingfield = $request->teachingfield;
        $faculty->researchinterest = $request->researchinterest;
        $faculty->email = $request->email;
        $faculty->studysubject = $request->studysubject;
        if(file_exists($request->file('image'))){
            $image = "faculty".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/faculty');
            $request->file('image')->move($location, $image);
            $faculty->image = $image;
        }
        else{
            $faculty->image = 'default-thumbnail.png';
        }        
        $faculty->save();
        return redirect('/home/faculty');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function show(Faculty $faculty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function edit(Faculty $faculty,$id)
    {
         $faculty = Faculty::findOrFail($id);
        return view ('dashboard.faculty.edit',compact('faculty'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faculty $faculty,$id)
    {
       $faculty = Faculty::findOrFail($id);
         $request->validate([
            'facultyname' => 'required',
            'facultyposition' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $faculty->facultyname = $request->facultyname;
        $faculty->faculty_priority = $request->faculty_priority;
        $faculty->facultyposition = $request->facultyposition;
        $faculty->teachingfield = $request->teachingfield;
        $faculty->researchinterest = $request->researchinterest;
        $faculty->email = $request->email;
        $faculty->studysubject = $request->studysubject;
        if(file_exists($request->file('image'))){
            $image = "faculty".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/faculty');
            $request->file('image')->move($location, $image);
            $faculty->image = $image;
        }
        else{
            $faculty->image = $faculty->image;
        }        
        $faculty->save();
        return redirect('/home/faculty');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faculty $faculty,$id)
    {
        $faculty = Faculty::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
