<?php

namespace App\Http\Controllers;

use App\Staff;
use Illuminate\Http\Request;

class StaffController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $staff = Staff::all();
        return view ('dashboard.staff.index',compact('staff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.staff.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $staff = new Staff();
        $request->validate([
            'name' => 'required',
            'position' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $staff->name = $request->name;
        $staff->position = $request->position;
        $staff->staff_priority = $request->staff_priority;
        if(file_exists($request->file('image'))){
            $image = "staff".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/staff');
            $request->file('image')->move($location, $image);
            $staff->image = $image;
        }
        else{
            $staff->image = 'default-thumbnail.png';
        }        
        $staff->save();
        return redirect('/home/staff');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function show(Staff $staff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function edit(Staff $staff,$id)
    {
         $staff = Staff::findOrFail($id);
        return view ('dashboard.staff.edit',compact('staff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Staff $staff, $id)
    {
       $staff = Staff::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'position' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $staff->name = $request->name;
        $staff->position = $request->position;
        $staff->staff_priority = $request->staff_priority;
        if(file_exists($request->file('image'))){
            $image = "staff".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/staff');
            $request->file('image')->move($location, $image);
            $staff->image = $image;
        }
        else{
            $staff->image = $staff->image;
        }        
        $staff->save();
        return redirect('/home/staff');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function destroy(Staff $staff,$id)
    {
         $staff = Staff::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
