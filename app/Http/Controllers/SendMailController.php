<?php

namespace App\Http\Controllers;

use App\SendMail;
use Illuminate\Http\Request;

class SendMailController extends Controller
{ 
     function sendmail(Request $request)
    {
    $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required|email',
      'subject' =>  'required',
      'phone' =>  'required',
      'message' =>  'required'
     ]);


        $data = array(
            'name'      =>  $request->name,
            'subject'   =>   $request->subject,
            'email'   =>   $request->email,
            'phone'   =>   $request->phone,
            'message'   =>   $request->message
        );

        $txt1 = '<html>
        <head>  
        </head>
        <body>
                    <p>Hi, This is '. $data['name'] .'</p>
                    <p>Email:'. $data['email'] .'<br><br>Phone No:'. $data['phone'] .'</p>
                    <p>Subject: '. $data['subject'] .' <br><br>
                    Message:<br>
                     '. $data['message'] .'.</p>
                    <p>It would be appriciative, if i receive the response soon.</p>
        </body>
        </html>';       

        $to = "bravo789charlie@gmail.com";
        $subject = "College Inquiry";

        $headers = "From:collegemtc.nepgeeks.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
        SendMail::create($request->all());
        $result=   mail($to,$subject,$txt1,$headers);
       return 'result is'.$result;
       }




    function send(Request $request)
    {
           $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required|email',
      'subject' =>  'required',
      'phone' =>  'required',
      'message' =>  'required'
     ]);
        $data = array(
            'name'      =>  $request->name,
            'subject'   =>   $request->subject,
            'email'   =>   $request->email,
            'phone'   =>   $request->phone,
            'message'   =>   $request->message
        );
       
        $txt2 = '<html>
        <head>  
        </head>
        <body>
                    <p>Hi, This is '. $data['name'] .'</p>
                    <p>Email:'. $data['email'] .'<br><br>Phone No:'. $data['phone'] .'</p>
                    <p>Subject: '. $data['subject'] .' <br><br>
                    Message:<br>
                     '. $data['message'] .'</p>
                    <p>It would be appriciative, if i receive the response soon.</p>
        </body>
        </html>';       

        $to = "bravo789charlie@gmail.com";
        $subject = "Contact for more Information";

        $headers = "From:collegemtc.nepgeeks.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
         SendMail::create($request->all());
        $result=   mail($to,$subject,$txt2,$headers);
        return back()->with('success','Thanks for Visiting us!');
        }


    public function index()
    {
       return view('contact');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Display the specified resource.
     *
     * @param  \App\SendMail  $sendMail
     * @return \Illuminate\Http\Response
     */
    public function show(SendMail $sendMail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SendMail  $sendMail
     * @return \Illuminate\Http\Response
     */
    public function edit(SendMail $sendMail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SendMail  $sendMail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SendMail $sendMail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SendMail  $sendMail
     * @return \Illuminate\Http\Response
     */
    public function destroy(SendMail $sendMail)
    {
        //
    }
}
