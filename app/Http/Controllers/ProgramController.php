<?php

namespace App\Http\Controllers;

use App\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programs = Program::all();
        return view ('dashboard.program.index',compact('programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.program.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $programs = new Program();
        $request->validate([
            'programname' => 'required',
            'qualification' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $programs->programname = $request->programname;
        $programs->qualification = $request->qualification;
        $programs->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "programs".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/programs');
            $request->file('image')->move($location, $image);
            $programs->image = $image;
        }
        else{
            $programs->image = 'default-thumbnail.png';
        }        
        $programs->save();
        return redirect('/home/programs');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program,$id)
    {
       $programs = Program::findOrFail($id);
        return view ('dashboard.program.edit',compact('programs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Program $program,$id)
    {
        $programs =Program::findOrFail($id);
        $request->validate([
            'programname' => 'required',
            'qualification' => 'required',
             'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $programs->programname = $request->programname;
        $programs->qualification = $request->qualification;
        $programs->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "programs".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/programs');
            $request->file('image')->move($location, $image);
            $programs->image = $image;
        }
        else{
            $programs->image = $programs->image;
        }        
        $programs->save();
        return redirect('/home/programs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy(Program $program,$id)
    {
        $programs = Program::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
