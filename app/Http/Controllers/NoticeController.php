<?php

namespace App\Http\Controllers;

use App\Notice;
use Illuminate\Http\Request;

class NoticeController extends Controller
{


     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notice = Notice::all();
        return view ('dashboard.notice.index',compact('notice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.notice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $notice = new Notice();
        $request->validate([
            'noticetitle' => 'required',
            'noticedescription' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $notice->noticetitle = $request->noticetitle;
        $notice->noticedescription = $request->noticedescription;
        if(file_exists($request->file('image'))){
            $image = "notice".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/notice');
            $request->file('image')->move($location, $image);
            $notice->image = $image;
        }
        else{
            $notice->image = 'default-thumbnail.png';
        }        
        $notice->save();
        return redirect('/home/notice');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function show(Notice $notice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function edit(Notice $notice,$id)
    {
         $notice = Notice::findOrFail($id);
        return view ('dashboard.notice.edit',compact('notice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notice $notice,$id)
    {
       $notice = Notice::findOrFail($id);
        $request->validate([
            'noticetitle' => 'required',
            'noticedescription' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $notice->noticetitle = $request->noticetitle;
        $notice->noticedescription = $request->noticedescription;
        if(file_exists($request->file('image'))){
            $image = "notice".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/notice');
            $request->file('image')->move($location, $image);
            $notice->image = $image;
        }
        else{
            $notice->image = $notice->image;
        }        
        $notice->save();
        return redirect('/home/notice');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notices = Notice::findOrFail($id)->delete();
        return redirect()->back();
    }
}
