<?php

namespace App\Http\Controllers;
use App\News;
use App\Notice;
use App\Program;
use App\Event;
use Carbon\Carbon;
use App\Gallery;
use App\Faculty;
use App\Staff;
use App\Slider;
use App\view_count; 
use Session;
use App\Alumuni;
use App\Popup;
use App\publication;
use Illuminate\Http\Request;

class MainPageController extends Controller
{
    public function index(){
      $viewCount = view_count::firstOrCreate([
      'ip' => $_SERVER['REMOTE_ADDR'],
      'session_id' => session()->getID()
    ]);

    $result = view_count::where('session_id', session()->getID())->first();

    if($result){

      $result->view_count += 1;
      $result->save();
    }
        $sliders=Slider::all();
        $programCount = Program::count();
    	$notices=Notice::latest()->get();
    	$news=News::Latest()->get();
    	$programs=Program::Latest()->get();
    	$events=Event::Latest()->get();
        $gallery=Gallery::Latest()->get();
        $modals=Popup::latest()->get();;
    	return view('index',compact('notices','sliders','news','programs','events','gallery','programCount','modals'));
    }
     public function about(){
    	return view('about');
    }
    public function overview(){
    	return view('overview');
    }

    public function historyMtc(){

        return view('historyMtc'); 
        
    }


    public function slientfeatures(){
    	return view('slientfeatures');
    }
    public function internalExamination(){
    	return view('internalexam');
    }
    public function admission(){
    	return view('admission');
    }
    public function faq(){
    	return view('faq');
    }
    public function testimonial(){
    	return view('testimonial');
    }
    public function rmc(){
    	return view('rmc');
    }

    public function publication(){
        $publications=publication::all();
        return view('research',compact('publications'));
    }

    public function alumuniassociation(){
        $alumunis=Alumuni::Latest()->paginate(6);
    	return view('almuniassociation',compact('alumunis'));
    }
    public function committee(){
    	return view('committee');
    }
    public function staff(){
        $staffs=Staff::orderBy('staff_priority','asc')->paginate(9);
        return view('staff',compact('staffs'));
    }
    public function contact(){
    	return view('contact');
    }
    public function gallery(){
        $gallery=Gallery::Latest()->get();
    	return view('gallery',compact('gallery'));
    }
    public function program(){
    	$programs=Program::Latest()->get();
    	return view('program',compact('programs'));
    }
    public function news(){
        $news=News::all();
        return view('news',compact('news'));
    }
    public function notice(){
        $notices=Notice::all();
        return view('notice',compact('notices'));
    }
    public function faculty(){
        $bpharm_faculty  = Faculty::where('teachingfield','<p>Bpharma</p>')->get();
        $bscmlt_faculty = Faculty::where('teachingfield','<p>BMLT</p>')->get();
        $bph_faculty = Faculty::where('teachingfield','<p>BPH</p>')->get();
        return view ('faculty',compact('bpharm_faculty','bscmlt_faculty','bph_faculty'));
    }
    public function organizationalchart(){
    	return view('organizationalchart');
    }
    public function messagefromchairman(){
        return view('messagefromchairman');
    }
    public function messagefromprincipal(){
        return view('messagefromprincipal');
    }
    public function eventdetails($id){
        $events = Event::findOrFail($id);
       return view('eventdetails',compact('events'));
    }
    public function newsdetails($id){
        $news = News::findOrFail($id);
       return view('newsdetails',compact('news'));
    }
    public function noticedetails($id){
        $notices = Notice::findOrFail($id);
       return view('noticedetails',compact('notices'));
    }
    public function programdetails($id){
        $programs = Program::findOrFail($id);
       return view('programdetails',compact('programs'));
    }
    public function facultydetails($id){
        $faculty = Faculty::findOrFail($id);
       return view('facultydetails',compact('faculty'));
    }
}
